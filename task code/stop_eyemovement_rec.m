%% Stop eye recording

% wait a while to record a few more samples
WaitSecs(.1);

% mark closing time in data file
Eyelink('Message', 'EXPSTOP');

% finish up: stop recording eye-movements and close file
Eyelink('StopRecording');
Eyelink('Command', 'set_idle_mode');

WaitSecs(.5);

Eyelink('CloseFile');


%% Transfer Eyelink data file

% Download data file and shut down tracker
try
    
    fprintf('Receiving data file ''%s''\n', filenameEDF);
    status = Eyelink('ReceiveFile');
    
    if status > 0
        fprintf('ReceiveFile status %d\n', status);
    end
    
    if 2 == exist(filenameEDF, 'file')
        fprintf('Data file ''%s'' can be found in ''%s''\n', filenameEDF);
    end
    
catch
    fprintf('Problem receiving data file ''%s''\n', filenameEDF);
end

Eyelink('ShutDown');