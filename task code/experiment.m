
clear all; close all; clc;
rand('state',sum(100*clock)); % set random generator to random seed
KbName('UnifyKeyNames');

% Ensure matlab has access to subdirectory with all required functions
global curWindow x0 y0 params dsgn stim data timing GetEyes reward

GetEyes = 0;

% Record software versions and time of session
SESSION.comp = computer;
SESSION.matlab_version = version;
[~, SESSION.ptb_version] = PsychtoolboxVersion;
SESSION.timing.session_start = datestr(now, 'YYYY-MM-DDThh:mm:ss'); % ISO8601 format

% Specify repetition time (used for calculation of offset period duration)
% and number of measurements (for comparing expected to actual run time)
SESSION.timing.TR = 2.2;
SESSION.n_measurements = 400;
SESSION.timing.scanning_duration_run = SESSION.timing.TR * SESSION.n_measurements;

%% Screen sync test
% Screen('Preference','SyncTestSettings', 0.01);%0.001 %set required accuracy of SyncTest
% Screen('Preference','SkipSyncTests', 1); %disable sync test

vpcode=inputdlg({'Subject ID'},'Subject ID',1,{'999'});
data.id=str2num(vpcode{1});
% age = inputdlg({'age'},'age',1,{'999'});
% data.age = 999%str2num(age{1});
% data.handedness = 'r'%questdlg('handedness', 'handedness', 'l', 'r', 'r');
% data.gender = 'f'%questdlg('gender', 'gender', 'f', 'm', 'f');

datafilename = [pwd, ['data\data_' num2str(data.id) '.txt']];
diary(['data\log\log_' num2str(data.id) '_' datestr(now, 'yyyymmdd_HHMMSS') '.txt'])

start_block = 1;
if start_block == 1
    reward = 0;
    save(['mat\reward\reward' num2str(data.id) '.mat'], 'reward');
end

%%% check if data already exists and query for overwrite if so
if exist(datafilename, 'file') > 0
    choice = questdlg(sprintf('Subject ID already exists \n\nOverwrite existing data??'), ...
        'overwrite data?','No','Yes', 'No');
    % Handle response
    switch choice
        case 'No'
            sca; clc;
            error('False subject ID')
    end
end

%% load randomized conditions
load(['mat\randomization\rand' num2str(data.id) '.mat'], 'trial_cond_rand');
%% columns
% 1 = trial condition (1 = diagonal target jump, 2 = vertical target jump, 3 = no target jump, 4  = short trials, 5 = ultra-short trials)
% 2 = choice option 1 (1 = face, 2 = house)
% 3 = choice option 2
% 4 = initial location (1 = left/right; 2 = above/below centre)
% 5 = initial assignment (1: option 1 = face | 2: option 1 = house)
% 6 = target distance (1 = close, 2 = far)
% 7 = target arrangement (1 = long, 2 = wide)
% 8 = position 1 (1 = ul, 2 = ur, 3 = lr, 4 = ll)
% 9 = position 2
%10 = ITI
%11 = Duration choice presentation
%12 = Delay after choice presentation
%13 = Delay after target relocation

load(['mat\reward\reward' num2str(data.id) '.mat'], 'reward');
load(['..\mat\RTcut\cut' num2str(data.id) '.mat'], 'cut');
load(['..\mat\tDist\tDist' num2str(data.id) '.mat'], 't_off');

%% open window
[curWindow, screenRect] = Screen('OpenWindow', 0, [0 0 0],[],32, 2);
[scrsz_x, scrsz_y] = Screen('WindowSize', 0);
x0 = scrsz_x/2; %define middle of screen/central fixation point as x zero point
y0 = scrsz_y/2;%+120; %define central fixation point as y zero point
hz=Screen('NominalFrameRate', curWindow); %75 Hz refresh rate?
Screen('TextSize', curWindow, 28); %30
Priority(MaxPriority(curWindow));

%% task parameters
dsgn.blocks = 5;%n blocks
dsgn.trials = 28;%trials per block

% dsgn.iti = [6000 2500 3000 9000]; %inter-trial interval (M SD Min Max)
% dsgn.DurChoicePres = [1500 500 1000 2000]; %duration of choice presentation (M SD Min Max)
% dsgn.delay = [6000 2500 3000 9000; 1500 1000 500 2500]; %delay between initial choice presentation and implementation (M SD Min Max)
% dsgn.delayCoM = [6000 2500 3000 9000]; %delay after target relocation ((M SD Min Max))
dsgn.dur_outcome = 800; %duration of outcome presentation
dsgn.colour_text = [210 210 210];
dsgn.speed = 60/hz;%[0.75 1.5]; %[slow medium fast] %how many pixels does manikin move for 1 keypress?
dsgn.jump = .5; %what portion of distance to initial target before relocation?
dsgn.CoMdead = 2000;

params.keys = [KbName('1!') KbName('3#') KbName('4$') KbName('2@')];%[y g r b]; %keys to move manikin: d(ul) k(ur) m(lr) c(ll)
params.exitkey = KbName('ESCAPE');%27; % WIN esc as exit code
params.contkey = 999;%KbName('w');%87; % WIN "w" to continue to next block
params.KeyMRItrigger = KbName('5%'); % Scanner Trigger
params.KeyRun = KbName('SPACE');

params.color_feedback = [150 30 30; 0 0 0];%red/white
params.manColour = [255 255 255; 0 150 0];%[150 30 30]; %white/green manikin
params.size_man = round(1*[64 90]); %size of manikin
params.target_offset = t_off;%[120 280; 280 120]; %x and y coordinates of targets for close and far targets
% params.target_offset(:,3) = sqrt(params.target_offset(1,1)^2 + params.target_offset(1,2)^2);%diagonal distance from centre of screen
params.target_size = [170 170];%[150 150];

params.target_locations(:,:,1) = [...
    [x0-params.target_offset(1,1)-params.target_size(1)/2 y0-params.target_offset(1,2)-(params.target_size(2))/2 x0-params.target_offset(1,1)+params.target_size(1)/2 y0-params.target_offset(1,2)+(params.target_size(2))/2];
    [x0+params.target_offset(1,1)-params.target_size(1)/2 y0-params.target_offset(1,2)-(params.target_size(2))/2 x0+params.target_offset(1,1)+params.target_size(1)/2 y0-params.target_offset(1,2)+(params.target_size(2))/2];
    [x0+params.target_offset(1,1)-params.target_size(1)/2 y0+params.target_offset(1,2)-(params.target_size(2))/2 x0+params.target_offset(1,1)+params.target_size(1)/2 y0+params.target_offset(1,2)+(params.target_size(2))/2];
    [x0-params.target_offset(1,1)-params.target_size(1)/2 y0+params.target_offset(1,2)-(params.target_size(2))/2 x0-params.target_offset(1,1)+params.target_size(1)/2 y0+params.target_offset(1,2)+(params.target_size(2))/2]];

params.target_locations(:,:,2) = [...
    [x0-params.target_offset(2,1)-params.target_size(1)/2 y0-params.target_offset(2,2)-(params.target_size(2))/2 x0-params.target_offset(2,1)+params.target_size(1)/2 y0-params.target_offset(2,2)+(params.target_size(2))/2];
    [x0+params.target_offset(2,1)-params.target_size(1)/2 y0-params.target_offset(2,2)-(params.target_size(2))/2 x0+params.target_offset(2,1)+params.target_size(1)/2 y0-params.target_offset(2,2)+(params.target_size(2))/2];
    [x0+params.target_offset(2,1)-params.target_size(1)/2 y0+params.target_offset(2,2)-(params.target_size(2))/2 x0+params.target_offset(2,1)+params.target_size(1)/2 y0+params.target_offset(2,2)+(params.target_size(2))/2];
    [x0-params.target_offset(2,1)-params.target_size(1)/2 y0+params.target_offset(2,2)-(params.target_size(2))/2 x0-params.target_offset(2,1)+params.target_size(1)/2 y0+params.target_offset(2,2)+(params.target_size(2))/2]];

params.payoff = [50 10 10 5]; %reward fast-track | loss fast-track | Intention Pursuit | CoM
params.payout = 5; %need 5 points for each cent

params.linespace = 2;
params.wrapat = 75;
params.vSpacing = 2.5;

params.nBlockVerb = 0; %never verbalise choice

%% load images
dsgn.n_stim = 28; % number of pictures for houses/faces each
for n = 1:dsgn.n_stim
    %faces (1:28)
    f = imread(['pics\stim\F_' num2str(n) '.jpg']);
    stim(n) = Screen('MakeTexture', curWindow, f);
    %houses (29:56)
    h = imread(['pics\stim\H_' num2str(n) '.jpg']);
    stim(n+dsgn.n_stim) = Screen('MakeTexture', curWindow, h);
end

[man map alpha] = imread('pics\manikin_small.png');
man(:,:,2) = alpha(:,:);
Screen('BlendFunction', curWindow, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
man = Screen('MakeTexture', curWindow, man);
pic_ins = imread('pics\pic_ins.jpg');
pic_ins = Screen('MakeTexture', curWindow, pic_ins);
fixation = imread('pics\fixation.jpg');
fixation = Screen('MakeTexture', curWindow, fixation);
fixation_short = imread('pics\fixation_short.jpg');
fixation_short = Screen('MakeTexture', curWindow, fixation_short);

%% data structure for experiment
data.trials_tot = nan(dsgn.blocks, dsgn.trials);
data.options = nan(dsgn.blocks, dsgn.trials, 2);
data.initLoc = nan(dsgn.blocks, dsgn.trials);
data.initAssign = nan(dsgn.blocks, dsgn.trials);
data.trial_cond = nan(dsgn.blocks, dsgn.trials);
data.choiceLoc = nan(dsgn.blocks, dsgn.trials, 2);
data.choiceReloc = nan(dsgn.blocks, dsgn.trials, 2);
data.init_comp = nan(dsgn.blocks, dsgn.trials);
data.targ_arr = nan(dsgn.blocks, dsgn.trials);
data.targ_dist = nan(dsgn.blocks, dsgn.trials);
data.t_jump = nan(dsgn.blocks, dsgn.trials);
data.initChc = nan(dsgn.blocks, dsgn.trials);
data.finalChc = nan(dsgn.blocks, dsgn.trials);
data.initChc_cat = nan(dsgn.blocks, dsgn.trials);
data.finalChc_cat = nan(dsgn.blocks, dsgn.trials);
data.initTarget = nan(dsgn.blocks, dsgn.trials);
data.finalTarget = nan(dsgn.blocks, dsgn.trials);
data.CoM = nan(dsgn.blocks, dsgn.trials);
data.rt_cut = nan(dsgn.blocks, dsgn.trials);
data.mt = nan(dsgn.blocks, dsgn.trials);
data.colKey = nan(dsgn.blocks, dsgn.trials);
data.respAccFast = nan(dsgn.blocks, dsgn.trials);
data.respAccInit = ones(dsgn.blocks, dsgn.trials);
data.respAccFin = ones(dsgn.blocks, dsgn.trials);
data.miss = nan(dsgn.blocks, dsgn.trials);
data.reward = nan(dsgn.blocks, dsgn.trials);
data.reward_accum = nan(dsgn.blocks, dsgn.trials);
data.nKey = zeros(dsgn.blocks, dsgn.trials);

% prepare output for time stamps
timing.fix1 = nan(dsgn.blocks, dsgn.trials);
timing.iti = nan(dsgn.blocks, dsgn.trials);
timing.choiceOn = nan(dsgn.blocks, dsgn.trials);
timing.DurChoicePres = nan(dsgn.blocks, dsgn.trials);
timing.fix2 = nan(dsgn.blocks, dsgn.trials);
timing.delay = nan(dsgn.blocks, dsgn.trials);
timing.targetsOn = nan(dsgn.blocks, dsgn.trials);
timing.rt_init = nan(dsgn.blocks, dsgn.trials);
timing.rt_init_err = nan(dsgn.blocks, dsgn.trials);
timing.targetsRel = nan(dsgn.blocks, dsgn.trials);
timing.delayCoM = nan(dsgn.blocks, dsgn.trials);
timing.CoMgo = nan(dsgn.blocks, dsgn.trials);
timing.rt_reloc = nan(dsgn.blocks, dsgn.trials);
timing.rt_reloc_err = nan(dsgn.blocks, dsgn.trials);
timing.tOutcome = nan(dsgn.blocks, dsgn.trials);

%% Experimental phase
HideCursor;
Screen('TextFont', curWindow, 'TimesNewRoman', 1);

%start EyeLink Recording
if GetEyes
    disp('Eyelink initiates. Calibrate.');
    start_eyemovement_rec;
    caliSucc = input('Calibration successful? y/n: ', 's'); %if calibration was successful, continue eytracking, otherwise, skip
    if caliSucc == 'n'
        GetEyes = 0;
    end
end
GetEyes

%start task
task;

%% save session info
SESSION.timing.session_end= datestr(now, 'YYYY-MM-DDThh:mm:ss'); % ISO8601 format
save(['data/session_' num2str(data.id)], 'SESSION');

% Stop Eyelink
if GetEyes
    stop_eyemovement_rec;
    disp('EyeLink was closed.');
end

%% end of experiment
sca

fprintf(['\n\n\n*****Reward: $' num2str(floor((reward/params.payout)/10)*10/100) '\n']);


Priority(0);
diary off;

