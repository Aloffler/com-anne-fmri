
id = 3%99;
load(['data\timing_' num2str(id) '.mat'], 'timing');

for run = 1:5
    subplot(5,1,run); hold on
    plot(timing.dummy_scans_abs(run,:),ones(1,5),'xr'); hold on
    plot(timing.run_start(run),1,'xb'); hold on
    plot(timing.run_start(run) + timing.fix1(run,:)/1000, ones(1,length(timing.fix1)),'+k'); hold on
    plot(timing.run_start(run) + timing.choiceOn(run,:)/1000, ones(1,length(timing.choiceOn)),'ok'); hold on
    plot(timing.run_start(run) + timing.targetsOn(run,:)/1000, ones(1,length(timing.targetsOn)),'dk'); hold on
    plot(timing.run_start(run) + timing.targetsOn(run,:)/1000 + timing.rt_init(run,:)/1000, ones(1,length(timing.rt_init)),'*k'); hold on
    plot(timing.run_start(run) + timing.targetsRel(run,:)/1000, ones(1,length(timing.targetsRel)),'dg'); hold on
    plot(timing.run_start(run) + timing.CoMgo(run,:)/1000, ones(1,length(timing.CoMgo)),'.g'); hold on
    plot(timing.run_start(run) + timing.CoMgo(run,:)/1000 + timing.rt_reloc(run,:)/1000, ones(1,length(timing.rt_reloc)),'*g'); hold on
end
