function [written] = writeout(datafile)
%WRITEOUT Write output after each block

global block trial data timing params dsgn

%% write output
%save as .mat
save(['data/data_' num2str(data.id)], 'data');

%save as txt
fid=fopen(datafile,'a');
if block == 1
    fprintf(fid, 'id\tblock\ttrial\ttr_tot\tcond\tinLoc\tinAss\topt1\topt2\tc1Loc\tc2Loc\tt_arr\tt_dist\tspeed\tdReloc\ttReloc\tc1Reloc\tc2Reloc\tinitTar\tfinTar\tiChc\tfChc\tiChcCat\tfChcCat\tCoM\tRTini\tRTiniE\tRTcut\tRTrel\tRTrelE\tMT\tkFast\tAccFast\tAccInit\tAccFin\tmiss\tnKey\treward\trewAcc\ttDistX\ttDistY\r');
    fprintf(fid,'\r\n');
end
for k=1:trial
    fprintf(fid, '\r\n%i', data.id);
    fprintf(fid, '\t%i', block);
    fprintf(fid, '\t%i', k);
    fprintf(fid, '\t%i', data.trials_tot(block,k)); %total trials
    fprintf(fid, '\t%i', data.trial_cond(block,k)); %trial condition
    fprintf(fid, '\t%i', data.initLoc(block,k)); %left/right (1) or above/below (2) fixation cross
    fprintf(fid, '\t%i', data.initAssign(block,k));%1: option 1 = face | 2: option 1 = house
    fprintf(fid, '\t%i', data.options(block,k,1)); %upper/left choice option (face/house)
    fprintf(fid, '\t%i', data.options(block,k,2)); %lower/righ choice option (face/house)
    fprintf(fid, '\t%i', data.choiceLoc(block,k, 1)); %target locatin of upper/left choice option
    fprintf(fid, '\t%i', data.choiceLoc(block,k, 2)); %target locatin of lower/right choice option
    fprintf(fid, '\t%i', data.targ_arr(block,k)); %target arrangment (long/wide)
    fprintf(fid, '\t%i', data.targ_dist(block,k)); %target distance (close/far)
    fprintf(fid, '\t%1.2f', dsgn.speed); %actual manikin speed + jitter
    fprintf(fid, '\t%1.1f', dsgn.jump); %distance after which targets jump
    fprintf(fid, '\t%1.0f', data.t_jump(block,k)); %time after which targets jump (measured from onset of manikin in centre -> -RTini = constant)
    fprintf(fid, '\t%i', data.choiceReloc(block,k, 1)); %color 1 - location after jump
    fprintf(fid, '\t%i', data.choiceReloc(block,k, 2)); %color 2 - location after jump
    fprintf(fid, '\t%i', data.initTarget(block,k)); %initial target choice
    fprintf(fid, '\t%i', data.finalTarget(block,k)); %final target choice
    fprintf(fid, '\t%i', data.initChc(block,k)); %initial choice
    fprintf(fid, '\t%i', data.finalChc(block,k)); %final choice
    fprintf(fid, '\t%i', data.initChc_cat(block,k)); %initial choice category
    fprintf(fid, '\t%i', data.finalChc_cat(block,k)); %final choice category
    fprintf(fid, '\t%i', data.CoM(block,k));
    fprintf(fid, '\t%1.0f', timing.rt_init(block,k));
    fprintf(fid, '\t%1.0f', timing.rt_init_err(block,k));
    fprintf(fid, '\t%1.0f', data.rt_cut(block,k));
    fprintf(fid, '\t%1.0f', timing.rt_reloc(block,k));
    fprintf(fid, '\t%1.0f', timing.rt_reloc_err(block,k));
    fprintf(fid, '\t%1.0f', data.mt(block,k));
    fprintf(fid, '\t%i', data.colKey(block,k));
    fprintf(fid, '\t%i', data.respAccFast(block,k));
    fprintf(fid, '\t%i', data.respAccInit(block,k));
    fprintf(fid, '\t%i', data.respAccFin(block,k));
    fprintf(fid, '\t%i', data.miss(block,k));
    fprintf(fid, '\t%i', data.nKey(block,k));
    fprintf(fid, '\t%i', data.reward(block,k));
    fprintf(fid, '\t%i', data.reward_accum(block,k));
    fprintf(fid, '\t%1.0f', params.target_offset(1,1));
    fprintf(fid, '\t%1.0f', params.target_offset(1,2));
end
fclose(fid);

%% write summary output
fid_sum=fopen(['data\data_sum' num2str(data.id) '.txt'],'a');
if block == 1
    fprintf(fid_sum, 'id\tblock\tnCoMdia\tnCoMpar\tnEarly\tpFace\tpError\tmissF\trunStart\r');
    fprintf(fid_sum,'\r\n');
end

fprintf(fid_sum, '\r\n%i', data.id);
fprintf(fid_sum, '\t%i', block);
fprintf(fid_sum, '\t%i', length(find(data.CoM(block,find(data.trial_cond(block,:)==1))==1)));
fprintf(fid_sum, '\t%i', length(find(data.CoM(block,find(data.trial_cond(block,:)==2))==1)));
fprintf(fid_sum, '\t%i', length(find(data.miss(block,find(data.trial_cond(block,:)<3))<0)));
fprintf(fid_sum, '\t%1.1f', 100*(length(find(data.initChc_cat(block,:) == 1))/trial));
fprintf(fid_sum, '\t%1.1f', 100*(length(find(data.respAccFast(block,find(data.trial_cond(block,:)>3))==0))+length(find(data.respAccInit(block,find(data.trial_cond(block,:)<4))==0)))/trial);
fprintf(fid_sum, '\t%1.1f', length(find(data.miss(block,find(data.trial_cond(block,:)>3))==1))*100/length(find(data.trial_cond(block,:)>3)));
fprintf(fid_sum, '\t%i', timing.run_start(block));
fclose(fid_sum);


%% save TIMING output
save(['data/timing_' num2str(data.id) '_' num2str(block)], 'timing');

written = block;

end

