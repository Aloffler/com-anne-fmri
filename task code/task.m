global block trial manCol run_start cont

for block = start_block:dsgn.blocks
    cont = 0;
    
    keyIsDown = 0;
    fprintf('\n\n\n***************Press Space Bar to start run!***************\n\n\n');
    while 1
        [keyIsDown, ~, keyCode] = KbCheck;
        if keyIsDown && find(keyCode,1,'first') == params.KeyRun
            break
        end
    end
    
    %% Collect dummy scans
    % Display response map while dummy scans are performed
    fprintf(['\n\n\nStarting run ' num2str(block) ' at ' datestr(now) '.\n']);
    
    DrawFormattedText(curWindow, 'Get ready...', 'center', 'center', [255 255 255]);
    Screen('Flip', curWindow);
    HideCursor;
    
    % Record time at start of run
    timing.run_launch(block) = GetSecs();
    if GetEyes
        Eyelink('Message', 'RUN');
    end
    n_dummy_scans = 5;
    dummy_times_absolute = dummyScans(n_dummy_scans, params.KeyMRItrigger);
    timing.dummy_scans_abs(block,:) = dummy_times_absolute;
    run_start = dummy_times_absolute(1);
    timing.dummy_scans_rel(block,:) = 1000*(dummy_times_absolute - run_start);
    
    timing.run_start(block) = run_start;
    
    RestrictKeysForKbCheck([]);
    %% START
    for trial = 1:dsgn.trials
        
        manCol = params.manColour(1,:);
        
        data.trials_tot(block, trial) = (block-1)*dsgn.trials+trial;
        data.trial_cond(block, trial) = trial_cond_rand(data.trials_tot(block, trial),1);
        data.targ_dist(block, trial) = trial_cond_rand(data.trials_tot(block, trial), 6); %target distance (1 = close, 2 = far)
        data.targ_arr(block, trial) = trial_cond_rand(data.trials_tot(block, trial), 7); %target arrangement (1 = long, 2 = wide)
        
        %choice location: assign 2 choice options to 2 neighbouring locations
        data.choiceLoc(block, trial, 1:2) = trial_cond_rand(data.trials_tot(block, trial),8:9);
        data.choiceReloc(block, trial, 1:2) = data.choiceLoc(block, trial, 1:2);
        
        timing.delay(block,trial) = trial_cond_rand(data.trials_tot(block, trial),12);
        
        fprintf(['Starting trial ' num2str(trial) '/' num2str(dsgn.trials) ' of run ' num2str(block) '.\n']);
        
        %% start trial by showing fixation cross for duration of ITI
        Screen('DrawTexture', curWindow, fixation);
        [~, ~, t_start] = Screen('Flip', curWindow, 0);
        if GetEyes
            Eyelink('Message', 'FIXI');
        end
        timing.fix1(block, trial) = 1000*(t_start - run_start);
        timing.iti(block, trial) = trial_cond_rand(data.trials_tot(block, trial),10);
        
        
        %% present 2 choice options left/right of centre after ITI
        data.options(block,trial,:) = [trial_cond_rand(data.trials_tot(block, trial),2) trial_cond_rand(data.trials_tot(block, trial),3)];
        data.initLoc(block, trial) = trial_cond_rand(data.trials_tot(block, trial),4);
        data.initAssign(block, trial) = trial_cond_rand(data.trials_tot(block, trial),5); %just for output (1: option 1 = face | 2: option 1 = house)
        
        Screen('DrawTexture', curWindow, fixation);
        if data.initLoc(block, trial) == 1
            Screen('DrawTexture', curWindow, stim(data.options(block,trial,1)), [], [x0-params.target_size(1)/2-120 y0-params.target_size(2)/2 x0+params.target_size(1)/2-120 y0+params.target_size(2)/2]);
            Screen('DrawTexture', curWindow, stim(data.options(block,trial,2)), [], [x0-params.target_size(1)/2+120 y0-params.target_size(2)/2 x0+params.target_size(1)/2+120 y0+params.target_size(2)/2]);
        else
            Screen('DrawTexture', curWindow, stim(data.options(block,trial,1)), [], [x0-params.target_size(1)/2 y0-params.target_size(2)/2-120 x0+params.target_size(1)/2 y0+params.target_size(2)/2-120]);
            Screen('DrawTexture', curWindow, stim(data.options(block,trial,2)), [], [x0-params.target_size(1)/2 y0-params.target_size(2)/2+120 x0+params.target_size(1)/2 y0+params.target_size(2)/2+120]);
        end
        [~, ~, t_choice] = Screen('Flip', curWindow, t_start + timing.iti(block, trial)/1000);
        if GetEyes
            Eyelink('Message', 'CHOPT');
        end
        timing.choiceOn(block, trial) = 1000*(t_choice - run_start);
        
        %% present blank screen after duration of choice presentation (1-2 s)
        timing.DurChoicePres(block, trial) = trial_cond_rand(data.trials_tot(block, trial),11);
        Screen('DrawTexture', curWindow, fixation);
        [~, ~, t_blank] = Screen('Flip', curWindow, t_choice + timing.DurChoicePres(block, trial)/1000);
        if GetEyes
            Eyelink('Message', 'FIXII');
        end
        timing.fix2(block, trial) = 1000*(t_blank -  run_start);
        
        if cont
            break
        end
        
        keyIsDown = 0;
        
        % trial condition
        % RT task
        if data.trial_cond(block, trial) > 3
            
            data.respAccInit(block, trial) = nan;
            data.respAccFin(block, trial) = nan;
            
            %% determine target-onset time
            data.rt_cut(block, trial) = cut;
            
            %choice options on green background
            Screen('DrawTexture', curWindow, fixation_short);
            Screen('DrawTexture', curWindow, stim(data.options(block,trial,1)), [], params.target_locations(data.choiceLoc(block, trial, 1),:,data.targ_arr(block, trial)));
            Screen('DrawTexture', curWindow, stim(data.options(block,trial,2)), [], params.target_locations(data.choiceLoc(block, trial, 2),:,data.targ_arr(block, trial)));
            [~, ~, t_RT_onset] = Screen('Flip', curWindow, t_blank+timing.delay(block, trial)/1000);
            if GetEyes
                Eyelink('Message', 'TARGFAST');
            end
            Screen('FillRect', curWindow, [0 0 0]);
            Screen('DrawTexture', curWindow, fixation_short);
            Screen('Flip', curWindow, t_RT_onset+200/1000);
            timing.targetsOn(block, trial) = 1000*(t_RT_onset - run_start);
            
            while 1
                [keyIsDown,t0a,keyCode] = KbCheck;
                if keyIsDown == 1 && ismember(find(keyCode, 1, 'first'), params.keys) %if participant presses key
                    if GetEyes
                        Eyelink('Message', 'RESPFAST');
                    end
                    timing.rt_init(block, trial) = 1000*(t0a - t_RT_onset);
                    data.colKey(block, trial) = find(params.keys == find(keyCode,1,'first'));
                    if ismember(data.colKey(block, trial), data.choiceLoc(block, trial, :))
                        data.initChc(block, trial) = data.options(block,trial,find(data.choiceLoc(block, trial,:) == data.colKey(block, trial)));
                        if data.initChc(block, trial) <= dsgn.n_stim
                            data.initChc_cat(block, trial) = 1; %face
                        else
                            data.initChc_cat(block, trial) = 2; %house
                        end
                    else
                        data.initChc(block, trial) = 99;
                        data.initChc_cat(block, trial) = 0;
                    end
                    break
                end
                
                if (GetSecs - t_RT_onset) > 2
                    data.miss(block, trial) = 1;
                    data.initChc(block, trial) = 0;
                    reward = reward - params.payoff(2);
                    data.reward(block, trial) = -params.payoff(2);
                    DrawFormattedText(curWindow, ['Too slow!\n\n-' num2str(params.payoff(2)) ' points'], 'center' , 'center', params.color_feedback(1,:));
                    break
                end
            end
            
            %correct?
            if data.miss(block, trial)~=1 && data.initChc(block, trial) ~= 99 && data.initChc(block, trial) ~= 0
                data.respAccFast(block, trial) = 1;
                if  timing.rt_init(block, trial) <= data.rt_cut(block, trial)
                    data.miss(block, trial) = 0;
                    reward = reward + params.payoff(1);
                    DrawFormattedText(curWindow, ['+' num2str(params.payoff(1)) ' points'], 'center' , 'center' , dsgn.colour_text);
                    data.reward(block, trial) = params.payoff(1);
                else
                    data.miss(block, trial) = 1;
                    reward = reward - params.payoff(2);
                    data.reward(block, trial) = -params.payoff(2);
                    DrawFormattedText(curWindow, ['Too slow!\n\n-' num2str(params.payoff(2)) ' points'], 'center' , 'center', params.color_feedback(1,:));
                end
            elseif data.miss(block, trial)~=1 && data.initChc(block, trial) == 99
                reward = reward - params.payoff(2);
                DrawFormattedText(curWindow, ['Wrong key!\n\n-' num2str(params.payoff(2))], 'center' , 'center' , params.color_feedback(1,:));
                data.reward(block, trial) = -params.payoff(2);
                data.respAccFast(block, trial) = 0;
                if  timing.rt_init(block, trial) > data.rt_cut(block, trial) %wrong + too slow
                    data.miss(block, trial) = 1;
                else
                    data.miss(block, trial) = 0;
                end
            end
            [~, ~, t_out] = Screen('Flip', curWindow);
            if GetEyes
                Eyelink('Message', 'OUTFAST');
            end
            timing.tOutcome(block, trial) = 1000*(t_out - run_start);
            WaitSecs(dsgn.dur_outcome/1000);
        else
            timing.delayCoM(block, trial) = trial_cond_rand(data.trials_tot(block, trial),13);
            
            %present choice options at target locations + manikin at central position
            Screen('DrawTexture', curWindow, stim(data.options(block,trial,1)), [], params.target_locations(data.choiceLoc(block, trial, 1),:,data.targ_arr(block, trial)));
            Screen('DrawTexture', curWindow, stim(data.options(block,trial,2)), [], params.target_locations(data.choiceLoc(block, trial, 2),:,data.targ_arr(block, trial)));
            Screen('DrawTexture', curWindow, man, [], [x0-floor(params.size_man(1)/2) y0-floor(params.size_man(2)/2) x0+ceil(params.size_man(1)/2) y0+ceil(params.size_man(2)/2)], [], [], 2, manCol);
            [~, ~, t_RT_onset] = Screen('Flip', curWindow, t_blank+timing.delay(block, trial)/1000);
            if GetEyes
                Eyelink('Message', 'TARG');
            end
            timing.targetsOn(block, trial) = 1000*(t_RT_onset - run_start);
            
            [timing.rt_init(block, trial), t_reloc, timing.rt_reloc(block, trial), data.mt(block, trial), ...
                data.initTarget(block, trial), data.finalTarget(block, trial), trajectories] ...
                = moveMan(fixation, man, t_RT_onset);
            
            if cont == 0
                data.initChc(block, trial) = data.options(block,trial,find(data.choiceLoc(block, trial,:) == data.initTarget(block, trial)));
                if data.initChc(block, trial) <= dsgn.n_stim
                    data.initChc_cat(block, trial) = 1; %face
                else
                    data.initChc_cat(block, trial) = 2; %house
                end
                
                % determine final choice
                if data.miss(block, trial) ~= 2
                    % upper left target
                    if (trajectories(end,6)-5 < 0 && trajectories(end,7)+5 > 0) && isempty(find(data.choiceReloc(block, trial, :) == 1)) == 0
                        data.finalTarget(block, trial) = 1;
                        %upper right target
                    elseif (trajectories(end,6)+5 > 0 && trajectories(end,7)+5 > 0) && isempty(find(data.choiceReloc(block, trial, :) == 2)) == 0
                        data.finalTarget(block, trial) = 2;
                        %lower right target
                    elseif (trajectories(end,6)+5 > 0 && trajectories(end,7)-5 < 0) && isempty(find(data.choiceReloc(block, trial, :) == 3)) == 0
                        data.finalTarget(block, trial) = 3;
                        %lower left target
                    elseif (trajectories(end,6)-5 < 0 && trajectories(end,7)-5 < 0) && isempty(find(data.choiceReloc(block, trial, :) == 4)) == 0
                        data.finalTarget(block, trial) = 4;
                    end
                    
                    Screen('DrawTexture', curWindow, stim(data.options(block,trial,1)), [], params.target_locations(data.choiceReloc(block, trial, 1),:,data.targ_arr(block, trial)));
                    Screen('DrawTexture', curWindow, stim(data.options(block,trial,2)), [], params.target_locations(data.choiceReloc(block, trial, 2),:,data.targ_arr(block, trial)));
                    Screen('FrameRect', curWindow, [255 255 255], params.target_locations(data.finalTarget(block, trial),:,data.targ_arr(block, trial)),6);
                    Screen('DrawTexture', curWindow, man, [], [x0+trajectories(end,6)-floor(params.size_man(1)/2) y0-trajectories(end,7)-floor(params.size_man(2)/2) x0+trajectories(end,6)+ceil(params.size_man(1)/2) y0-trajectories(end,7)+ceil(params.size_man(2)/2)], [], [], 2, manCol);
                end
                
                % for long trials, write within-trial output
                if data.trial_cond(block, trial) < 4
                    
                    if ismember(data.finalTarget(block, trial), data.choiceReloc(block, trial,:))
                        data.finalChc(block, trial) = data.options(block,trial,find(data.choiceReloc(block, trial,:) == data.finalTarget(block, trial)));
                        if data.finalChc(block, trial) <= dsgn.n_stim
                            data.finalChc_cat(block, trial) = 1; %face
                        else
                            data.finalChc_cat(block, trial) = 2; %house
                        end
                        if data.miss(block, trial) ~= 2
                            if data.finalChc(block, trial) == data.initChc(block, trial)
                                data.CoM(block, trial) = 0;
                                DrawFormattedText(curWindow, ['+' num2str(params.payoff(3))], 'center' , 'center' , dsgn.colour_text);
                                reward = reward + params.payoff(3);
                                data.reward(block, trial) = params.payoff(3);
                            else
                                data.CoM(block, trial) = 1;
                                DrawFormattedText(curWindow, ['+' num2str(params.payoff(4))], 'center' , 'center' , dsgn.colour_text);
                                reward = reward + params.payoff(4);
                                data.reward(block, trial) = params.payoff(4);
                            end
                            [~, ~, t_out] = Screen('Flip', curWindow, 0);
                            if GetEyes
                                Eyelink('Message', 'OUT');
                            end
                            WaitSecs(dsgn.dur_outcome/1000);
                            timing.tOutcome(block, trial) = 1000*(t_out - run_start);
                        end
                    end
                end
            end
        end
        
        data.reward_accum(block, trial) = reward;
        
        if data.trial_cond(block, trial) >= 3
            data.d_jump(block, trial) = nan; %set back to nan for output
            data.choiceReloc(block,trial,:) = nan;
        elseif data.trial_cond(block, trial) < 3
            data.t_jump(block, trial) = 1000*(t_reloc-t_RT_onset);
            timing.targetsRel(block, trial) = 1000*(t_reloc - run_start);
        end
        
        data.initChc_cat(block, trial)
        
        if cont
            break
        end
        
    end
    
    Screen('DrawTexture', curWindow, fixation);
    Screen('Flip', curWindow, 0);
    
    % Wait for offset period
    fprintf(['Commencing offset period at ' datestr(now) '.\n']);
    WaitSecs(5*SESSION.timing.TR);
    
    % Calculate run duration
    timing.run_stop(block) = GetSecs() - run_start;
        fprintf(['\n\n\nStarting run ' num2str(block) ' at ' datestr(now) '.\n']);

    fprintf(['\nRun completed at ' datestr(now) '!\n' ...
        'Total duration = ' num2str(timing.run_stop(block)) ' sec (' num2str(round(10*((timing.run_stop(block))/60))/10) ' min)\n']);
    
    % Check for discrepancy between run duration and scanning duration
    timing.discrepancy(block) = SESSION.timing.scanning_duration_run - timing.run_stop(block);
    
    % Print timing discrepancy
    fprintf(['Scanning time remaining = ' num2str(timing.discrepancy(block)) ' seconds\n' ...
        '(If negative: scanning time too short!)\n\n']);
    
    if block == dsgn.blocks %after last block
        if reward > 0
            DrawFormattedText(curWindow, ['End of experiment.\n\n\nCongratulations, you won ' num2str(((reward))) ' points (= $' num2str((floor((reward/params.payout)/10)*10)/100) '0)'],  'center', 'center', dsgn.colour_text);
        else
            DrawFormattedText(curWindow, 'End of experiment.\n\n\nYou won $0.00.',  'center', 'center', dsgn.colour_text);
        end
    else
        DrawFormattedText(curWindow, ['End of block ' num2str(block) '. You can take a break now. Current score: ' num2str(((reward))) ' points (= $' num2str(round(100*reward/(params.payout*100))/100) ')'], 'center', scrsz_y/2-200, dsgn.colour_text);
        
        %too many/few CoM trials?
        if length(find(data.CoM(block,  find(data.trial_cond(block,:)==1)) == 1)) < 6 || length(find(data.CoM(block,  find(data.trial_cond(block,:)==1)) == 1)) > 10 || length(find(data.CoM(block,  find(data.trial_cond(block,:)==2)) == 1)) > 1
            DrawFormattedText(curWindow, 'Carefully consider each time if it is worth pursuing your initial choice!', 'center', scrsz_y/2-100, params.color_feedback(1,:),params.wrapat);
        end
        
        %too many early CoM responses?
        if length(find(data.miss(block,find(data.trial_cond(block,:)<3))<0)) > 2
            DrawFormattedText(curWindow, 'When the targets jump, wait for the manikin to turn green!', 'center', scrsz_y/2-25, params.color_feedback(1,:),params.wrapat);
        end
        
        %check if relatively balanced face/house choice
        if 100*(length(find(data.initChc_cat(block,:) == 1))/trial) < 35 || 100*(length(find(data.initChc_cat(block,:) == 1))/trial) > 65
            DrawFormattedText(curWindow, 'Try to choose faces and houses roughly equally often!', 'center', scrsz_y/2+50, params.color_feedback(1,:),params.wrapat);
        end
        
        %too many location errors?
        if 100*(length(find(data.respAccFast(block,find(data.trial_cond(block,:)>3))==0))+length(find(data.respAccInit(block,find(data.trial_cond(block,:)<4))==0)))/trial >= 10
            DrawFormattedText(curWindow, 'Try to be more accurate!', 'center', scrsz_y/2+125, params.color_feedback(1,:),params.wrapat);
        end
        
        %too many misses in short trials?
        if (length(find(data.miss(block,find(data.trial_cond(block,:)>3))==1))*100/length(find(data.trial_cond(block,:)>3))) > 50
            DrawFormattedText(curWindow, 'Respond more quickly in fast trials!', 'center', scrsz_y/2+200, params.color_feedback(1,:),params.wrapat);
        end
    end
    Screen('Flip',curWindow);
    
    %% write_output && save matrices
    save(['mat\reward\reward' num2str(data.id) '.mat'], 'reward');
    LastBlockSaved = writeout(['data\data_' num2str(data.id) '.txt']);
    
    %% check performance summary
    fprintf(['\n*****PERFORMANCE SUMMARY (RUN ' num2str(block) ')*****']);
    fprintf(['\nn CoM: ' num2str(length(find(data.CoM(block,find(data.trial_cond(block,:)==1))==1))) ' (6-10)']);
    fprintf(['\nn CoM catch: ' num2str(length(find(data.CoM(block,find(data.trial_cond(block,:)==2))==1))) ' (<2)']);
    fprintf(['\nn too early CoM: ' num2str(length(find(data.miss(block,find(data.trial_cond(block,:)<3))<0))) ' (<3)']);
    fprintf(['\n%% face: ' num2str(100*(length(find(data.initChc_cat(block,:) == 1))/trial)) ' (35-65)']);
    fprintf(['\n%% error: ' num2str(100*(length(find(data.respAccFast(block,find(data.trial_cond(block,:)>3))==0))+length(find(data.respAccInit(block,find(data.trial_cond(block,:)<4))==0)))/trial) ' (<10)']);
    fprintf(['\n%% miss fast: ' num2str(length(find(data.miss(block, find(data.trial_cond(block,:)>3))==1))*100/length(find(data.trial_cond(block,:)>3))) ' (<60)']);
    fprintf(['\n\n']);
    
    WaitSecs(2);
 
end

