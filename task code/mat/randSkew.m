M = 4500;
SD = 2000;
Min = 3000;
Max = 9000;
skew = .2;
kurt = 5;

random = pearsrnd(M,SD,skew,kurt,10000,1);

random(find(random<Min)) = [];
random(find(random>Max)) = [];

hist(random);
mean(random)