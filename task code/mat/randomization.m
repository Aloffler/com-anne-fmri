clear all
clc
rand('state',sum(100*clock)); % set random generator to random seed

%% load jitters
load jit

for id = 1:40
    
    clearvars -except id jit
    trial_cond_rand = [];
    
    dsgn.blocktrials = 28; %trials per block CAN DIVIDE BY 12 (condition ratio) x 8 (target locations)?
    dsgn.blocks_exp = 5;%number of blocks
    dsgn.trials = dsgn.blocktrials*dsgn.blocks_exp;
    
    %% trial conditions
    %1 = goal pursuit WITH target relocation (diagonal)
    %2 = goal pursuit WITH target relocation (vertical)
    %3 = goal pursuit WITHOUT target relocation
    %4 = RT short trials
    %5 = RT ultra-short trials
    dsgn.condPerc = [16 4 4 2 2]; %n trials per condition
    
    for block = 1:dsgn.blocks_exp
        trial_cond = [repmat(1,dsgn.condPerc(1),1); repmat(2,dsgn.condPerc(2),1); repmat(3,dsgn.condPerc(3),1); repmat(4,dsgn.condPerc(4),1); repmat(5,dsgn.condPerc(5),1)];
        
        
        %% target distance x arrangment
        t_dist_arr = [1 1; 1 2; 2 1; 2 2];
        %first 16 trials fixed (diagonal reloc)
        t_dist_arr_rand = repmat(kron([t_dist_arr],ones(4,1)),1,1);
        %remaining trials: counterbalance every 4 trial
        for k = 1:3
            t_dist_arr_rand = [t_dist_arr_rand; t_dist_arr(randperm(4),:)];
        end
       
        %% actual target locations
        %1 = upper/left, 2 = lower/right targets (depending on distance x arrangement)
        index_horizontal = find((t_dist_arr_rand(:,1)==t_dist_arr_rand(:,2))); %close&long or far&wide -> horizontal targets
        index_vertical = find((t_dist_arr_rand(:,1)~=t_dist_arr_rand(:,2))); %close&wide or far&close -> vertical targets
        %alternate assigning 1/2 to all horizontal (upper/lower) and 3/4 vertical (left/right) actual targets
        loc_horizontal = [1 2; 2 1; 3 4; 4 3];
        loc_vertical = [2 3; 3 2; 1 4; 4 1];
        loc_horizontal_rand = [];
        loc_vertical_rand = [];
        for k = 1:ceil(length(index_vertical)/length(loc_vertical))
            loc_horizontal_rand = [loc_horizontal_rand; loc_horizontal(randperm(4),:)];
            loc_vertical_rand = [loc_vertical_rand; loc_vertical(randperm(4),:)];
        end
        
        loc = nan(dsgn.blocktrials, 2);
        loc(index_horizontal, :) = loc_horizontal_rand(1:length(index_horizontal),:);
        loc(index_vertical, :) = loc_vertical_rand(1:length(index_vertical),:);
        
        %% choice options
        % 28 houses + 28 faces ==> showing each option once
        % assign to option 1 vs. 2 according to initial location
        
        %% initial target arrangement
        %randomly assign for 4 trials each
        init_loc = [1 1; 1 2; 2 1; 2 2];
        init_loc_rand = [];
        for k = 1:dsgn.blocktrials/length(init_loc)
            init_loc_rand = [init_loc_rand; init_loc(randperm(4),:)];
        end
        
        n_choice = 28;
        choice = [randperm(n_choice)' n_choice+randperm(n_choice)'];
        for k = 1:n_choice
            if init_loc_rand(k,2) == 2
                choice(k,:) = [choice(k,2) choice(k,1)];
            end
        end
        
        %add everything
        trial_cond_all = [trial_cond choice init_loc_rand(:,1:2) t_dist_arr_rand loc];
        trial_cond_rand_block = trial_cond_all(randperm(size(trial_cond_all,1)),:);

        trial_cond_rand = [trial_cond_rand; trial_cond_rand_block];
    end

    %% diagonal targets for short trials
    for k = 1:length(trial_cond_rand)
        if trial_cond_rand(k,1) > 3
            if trial_cond_rand(k,8:9) == [1 2]
                trial_cond_rand(k,8:9) = [1 3];
            elseif trial_cond_rand(k,8:9) == [2 1]
                trial_cond_rand(k,8:9) = [3 1];
            elseif trial_cond_rand(k,8:9) == [2 3]
                trial_cond_rand(k,8:9) = [2 4];
            elseif trial_cond_rand(k,8:9) == [3 2]
                trial_cond_rand(k,8:9) = [4 2];
            elseif trial_cond_rand(k,8:9) == [3 4]
                trial_cond_rand(k,8:9) = [3 1];
            elseif trial_cond_rand(k,8:9) == [4 3]
                trial_cond_rand(k,8:9) = [1 3];
            elseif trial_cond_rand(k,8:9) == [1 4]
                trial_cond_rand(k,8:9) = [2 4];
            elseif trial_cond_rand(k,8:9) == [4 1]
                trial_cond_rand(k,8:9) = [4 2];
            end
        end
    end
    
    %% add randomised jitters
    trial_cond_rand = [trial_cond_rand jit.ITI(randperm(dsgn.trials))' jit.ChoicePres(randperm(dsgn.trials))'];
    trial_cond_rand = [trial_cond_rand nan(length(trial_cond_rand),2)];
    
    
    %% delays for long+short vs. ultra-short trials
    Delay_long = jit.Delay(randperm(dsgn.trials-dsgn.condPerc(5)*dsgn.blocks_exp));
    Delay_ultraShort = jit.Delay(dsgn.trials-dsgn.condPerc(5)*dsgn.blocks_exp + randperm(dsgn.condPerc(5)*dsgn.blocks_exp));
    index_long = find(trial_cond_rand(:,1) < 5);
    index_ultraShort = find(trial_cond_rand(:,1) == 5);
    trial_cond_rand(index_long,12) = Delay_long;
    trial_cond_rand(index_ultraShort,12) = Delay_ultraShort;
    
    
    %% delays for relocation trials
    index_reloc = find(trial_cond_rand(:,1) < 3);
    Delay_reloc = jit.DelayCoM(randperm(length(jit.DelayCoM)));
    trial_cond_rand(index_reloc,13) = Delay_reloc;
    
    %% save as matrix
    save(['randomization\rand' num2str(id) '.mat'], 'trial_cond_rand');
    %%columns
    % 1 = trial condition (1 = diagonal target jump, 2 = vertical target jump, 3 = no target jump, 4  = short trials, 5 = ultra-short trials)
    % 2 = choice option 1 (1 = face, 2 = house)
    % 3 = choice option 2
    % 4 = initial location (1 = left/right; 2 = above/below centre)
    % 5 = initial assignment (1: option 1 = face | 2: option 1 = house)
    % 6 = target distance (1 = close, 2 = far)
    % 7 = target arrangement (1 = long, 2 = wide)
    % 8 = position 1 (1 = ul, 2 = ur, 3 = lr, 4 = ll)
    % 9 = position 2
    %10 = ITI
    %11 = Duration choice presentation
    %12 = Delay after choice presentation
    %13 = Delay after target relocation
end