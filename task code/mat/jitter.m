function [jit] = jitter(jitterDistribution)
%JITTER creates random jitter drawn from normal distribution with M/SD/Min/Max
%input: jitterDistribution = [M SD Min Max]
draw = 1;
skew = .2;
kurt = 5;

while draw
    jit = jitterDistribution(1) + jitterDistribution(2)*randn;
    if length(jitterDistribution) > 2%constraint by min and max (only if provided)
        if jit >= jitterDistribution(3) && jit <= jitterDistribution(4)
            draw = 0;
        end
    else
        draw = 0;
    end
end

end

% pd = makedist('normal','mu',0,'sigma',.2);
% while 1
%     data.speed(block, trial) = dsgn.speed(data.speed_cond(block, trial)) + random(pd,1);
%     if data.speed(block, trial) > .75 && data.speed(block, trial) < 1.25
%         break
%     end
% end
