%% create jitters
clear all
clc
rand('state',sum(100*clock)); % set random generator to random seed

dsgn.ITI = [4500 2000 3000 9000]; %inter-trial interval (M SD Min Max)
dsgn.ChoicePres = [1500 500 1000 2000]; %duration of choice presentation (M SD Min Max)
dsgn.Delay = [4500 2000 3000 9000; 1500 1000 500 2500]; %delay between initial choice presentation and implementation (M SD Min Max)
dsgn.DelayCoM = [4500 2000 3000 9000]; %delay after target relocation ((M SD Min Max))

dsgn.trials = 5*28; %trials 

for i = 1:dsgn.trials
    jit.ITI(i) = jitter(dsgn.ITI);
    jit.ChoicePres(i) = jitter(dsgn.ChoicePres);
    %differentiate between short and ultra-short trials
    if i <= 130 %130/140 trials with long delay (10 ultra-short trials)
        jit.Delay(i) = jitter(dsgn.Delay(1,:));
    else
        jit.Delay(i) = jitter(dsgn.Delay(2,:));
    end
end

for i = 1:100 %only assign random delay after relocation for relocation trials (conditions 1 & 2 = 100 trials)
    jit.DelayCoM(i) = jitter(dsgn.DelayCoM);
end

save('jit');

subplot(2,2,1); hist(jit.ITI); xlabel('ITI'); set(gca, 'XLim',[dsgn.ITI(3)-1000 dsgn.ITI(4)+1000]);
subplot(2,2,2); hist(jit.ChoicePres); xlabel('Duration choice presentation'); set(gca, 'XLim',[dsgn.ChoicePres(3)-1000 dsgn.ChoicePres(4)+1000]);
subplot(2,2,3); hist(jit.Delay); xlabel('Delay after choice presentation'); set(gca, 'XLim',[dsgn.Delay(2,3)-1000 dsgn.Delay(1,4)+1000]);
subplot(2,2,4); hist(jit.DelayCoM); xlabel('Delay after target relocation'); set(gca, 'XLim',[dsgn.DelayCoM(3)-1000 dsgn.DelayCoM(4)+1000]);

means = [mean(jit.ITI) mean(jit.ChoicePres) mean(jit.Delay) mean(jit.DelayCoM)]
sums = [max(jit.ITI) max(jit.ChoicePres) max(jit.Delay) max(jit.DelayCoM)]