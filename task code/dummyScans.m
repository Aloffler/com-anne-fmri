function [timestamps] = dummyScans(n, KeyTrigger)
% Collect n dummy scans

disp('Waiting for dummy scans...')

trigger_count = 0;
timestamps = nan(1, n);

% Listen for scanner trigger, not other events
RestrictKeysForKbCheck(KeyTrigger);

while trigger_count < n
    
    [keyIsDown, ~, keyCode] = KbCheck;
    
    if keyIsDown && find(keyCode) == KeyTrigger
        
        trigger_count = trigger_count + 1;
        timestamps(trigger_count) = GetSecs();
        
        fprintf(['\tTrigger number ' num2str(trigger_count) ' received\n']);
        
        % Wait until Keys are released
        KbReleaseWait;
        
    end
    
end

disp('Dummy volumes collected.');

FlushEvents;

end