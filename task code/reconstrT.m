load('data\timing_3.mat');
load('data\data_3.mat');

timing.cond = data.trial_cond;

timing.fix1(1,1) = 8971.53218838139;
timing.fix1(2,1) = 8835.92896534174;

for block = 1:2
    for trial = 1:28
        timing.choiceOn(block, trial) = 2.275 + (timing.fix1(block,trial)+timing.iti(block,trial));
        timing.fix2(block,trial) =  2.544 + (timing.choiceOn(block,trial)+timing.DurChoicePres(block,trial));
        timing.targetsOn(block,trial) = 2.143 + (timing.fix2(block,trial)+timing.delay(block,trial));
        timing.targetsRel(block, trial) = 2567.5 + (timing.targetsOn(block,trial)+timing.rt_init(block,trial));
        timing.CoMgo(block, trial) = 4.0992 + (timing.targetsRel(block,trial)+timing.delayCoM(block,trial));
        
        if data.trial_cond(block,trial) < 4
            timing.tOutcome(block, trial) = 1000/60 + (timing.targetsOn(block,trial)+data.mt(block,trial));
        else %for short trials
            timing.tOutcome(block, trial) = 1000/60 + (timing.targetsOn(block,trial)+timing.rt_init(block,trial));
        end
        
        if trial < 28
            timing.fix1(block, trial+1) = 4.4175 + timing.tOutcome(block, trial) + 800; %duration outcome
        end
    end
end
    

%% check if delays sum up to total duration of run
sec = timing.tOutcome(:,end)/1000 + 11 %add offset period

save('timing_rec2', 'timing');