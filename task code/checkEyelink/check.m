clear all; close all; clc;
rand('state',sum(100*clock)); % set random generator to random seed
KbName('UnifyKeyNames');

global GetEyes params

GetEyes = 1;

id = input('ID: ');
DoFeedback = 1;%input('feedback? (1/0): ');

%% Screen sync test
Screen('Preference','SkipSyncTests', 1); %disable sync test
%% open window
[curWindow, screenRect] = Screen('OpenWindow', 2, [0 0 0],[],32, 2);
[scrsz_x, scrsz_y] = Screen('WindowSize', 2)
x0 = scrsz_x/2; %define middle of screen/central fixation point as x zero point
y0 = scrsz_y/2;%+120; %define central fixation point as y zero point
hz=Screen('NominalFrameRate', curWindow) %75 Hz refresh rate?
Screen('TextSize', curWindow, 28); %30
Priority(MaxPriority(curWindow));

params.keys = [37, 39];
params.target_offset = [280 0; 280 0]; %x and y coordinates of targets for close and far targets
params.target_size = [170 170];%[150 150];

params.target_locations(:,:,1) = [...
    [x0-params.target_offset(1,1)-params.target_size(1)/2 y0-params.target_offset(1,2)-(params.target_size(2))/2 x0-params.target_offset(1,1)+params.target_size(1)/2 y0-params.target_offset(1,2)+(params.target_size(2))/2];
    [x0+params.target_offset(1,1)-params.target_size(1)/2 y0-params.target_offset(1,2)-(params.target_size(2))/2 x0+params.target_offset(1,1)+params.target_size(1)/2 y0-params.target_offset(1,2)+(params.target_size(2))/2];
    [x0+params.target_offset(1,1)-params.target_size(1)/2 y0+params.target_offset(1,2)-(params.target_size(2))/2 x0+params.target_offset(1,1)+params.target_size(1)/2 y0+params.target_offset(1,2)+(params.target_size(2))/2];
    [x0-params.target_offset(1,1)-params.target_size(1)/2 y0+params.target_offset(1,2)-(params.target_size(2))/2 x0-params.target_offset(1,1)+params.target_size(1)/2 y0+params.target_offset(1,2)+(params.target_size(2))/2]];


%% Experimental phase
HideCursor;
Screen('TextFont', curWindow, 'TimesNewRoman', 1);

%start EyeLink Recording
if GetEyes
    disp('Eyelink initiates. Calibrate.');
    start_eyemovement_rec;
    caliSucc = input('Calibration successful? y/n: ', 's'); %if calibration was successful, continue eytracking, otherwise, skip
    if caliSucc == 'n'
        GetEyes = 0;
    end
end

%start task
HideCursor;
WaitSecs(2);


if GetEyes
    Eyelink('Message', 'START');
end

t_start = GetSecs;

%% START
for trial = 1:10
    
    %% start trial by showing fixation cross for duration of ITI
    DrawFormattedText(curWindow, '+', 'center', 'center', [255 255 255]);
    [~, ~, t_fix] = Screen('Flip', curWindow, 0);
    if GetEyes
        Eyelink('Message', 'FIX1');
    end
    data.t_fix(trial) = 1000*(t_fix - t_start);
    
    WaitSecs(1);
    %% present 2 choice options left/right of centre after ITI
    data.loc(trial) = randperm(2,1);
    DrawFormattedText(curWindow, '+', 'center', 'center', [255 255 255]);
    Screen('FillRect', curWindow, [255 255 255], params.target_locations(data.loc(trial),:,1))
    [~, ~, t_stim] = Screen('Flip', curWindow, 0);
    if GetEyes
        Eyelink('Message', 'TOn');
    end
    data.t_stim(trial) = 1000*(t_stim - t_start);
    
    keyIsDown = 0;
    while 1
        [keyIsDown,t0a,keyCode] = KbCheck;
        if keyIsDown == 1 %&& ismember(find(keyCode, 1, 'first'), params.keys) %if participant presses key
            if GetEyes
                Eyelink('Message', 'RESP');
            end
            if find(keyCode, 1, 'first') == 27
                sca
                break
            else
                data.key(trial) = find(keyCode, 1, 'first');
                if DoFeedback
                    Screen('FillRect', curWindow, [255 255 255], params.target_locations(data.loc(trial),:,1))
                    t_feed = feedback(data.key(trial), curWindow);
                    data.t_feed(trial) = 1000*(t_feed- t_start);
                end
                
                DrawFormattedText(curWindow, '+', 'center', 'center', [255 255 255]);
                [~, ~, t_fix2] = Screen('Flip', curWindow, 0);
                
                if GetEyes
                    Eyelink('Message', 'FIX2');
                end
                data.t_fix2(trial) = 1000*(t_fix2 - t_start);
                data.t_rt(trial) = 1000*(t0a - t_stim);
            end
            break
        end
    end
end

save(['data_' num2str(id) '.mat'], 'data');

% Stop Eyelink
if GetEyes
    stop_eyemovement_rec;
    disp('EyeLink was closed.');
end

%% end of experiment
sca

Priority(0);
diary off;

