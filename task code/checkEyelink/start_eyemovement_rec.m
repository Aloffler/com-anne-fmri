%% Create unique Eyelink filename
filenameEDF = [num2str(id) '.edf'];

%% Configure eye tracker & run calibration/validation
% Configurate Eeyetracker & run calibration / validation
EyelinkInit(0); % initialize the eyetracker
Eyelink('Command', 'link_sample_data = LEFT,RIGHT,GAZE,AREA'); % make sure it's gaze data

% Set eyetracking defaults
el = EyelinkInitDefaults(curWindow);

% Set colours for calibration
el.calibrationtargetcolour = WhiteIndex(el.window); % dot colour???
el.backgroundcolour = BlackIndex(el.window); % background colour
el.foregroundcolour = BlackIndex(el.window); % dot colour???
el.msgfontcolour    = WhiteIndex(el.window);
el.imgtitlecolour = WhiteIndex(el.window);
    
% need to call this to apply changes to default setting???
% EyelinkUpdateDefaults(el);

if ~isempty(el.window) && ~isempty(el.callback)
    PsychEyelinkDispatchCallback(el); % then pass it to the callback
end

status = Eyelink('openfile', filenameEDF); % open file to record data

fprintf('Press c/v to start calibration/validation');

% 1) [result, reply]=Eyelink('ReadFromTracker','enable_automatic_calibration');
% 2) Press A on calibration screen to accept fixation automatically

EyelinkDoTrackerSetup(el); % calibration and validation shown on screen
WaitSecs(0.1); % give it some time to catch up


%% Start recording eye data
fprintf('Start recording eye data');
% start recording eye position
Eyelink('StartRecording');
% record a few samples before we actually start displaying
WaitSecs(.1);
% mark zero-plot time in data file
Eyelink('Message', 'EXPSTART');
% record a few samples before we actually start displaying
WaitSecs(.1);