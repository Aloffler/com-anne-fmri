function [t_feed] = feedback(key, curWindow)
%FEEDBACK Summary of this function goes here
%   Detailed explanation goes here

global GetEyes params

% Screen('FrameRect', curWindow, [255 0 0], params.target_locations(key,:,1),4)

DrawFormattedText(curWindow, '+', 'center', 'center', [255 0 0]);
[~, ~, t_feed] = Screen('Flip', curWindow, 0);
if GetEyes
    Eyelink('Message', 'FEEDBACK');
end

WaitSecs(.5);

end

