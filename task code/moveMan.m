function [rt_init, t_reloc, rt_reloc, mt, init_targ, fin_targ, trajectories] = moveMan(fixation, man, t_RT_onset)
%MOVE_MAN Move manikin towards chosen targets

global block trial curWindow x0 y0 params dsgn stim data timing manCol run_start GetEyes cont

rt_init = nan;
t_reloc = nan;
rt_reloc = nan;
mt = nan;
init_targ = 0;
fin_targ = 0;
trajectories = [];%nan(1000, 7); %create empty matrix for trajectories of manikin (what's maximum duration?)
%1 = trial
%2 = sample
%3 = time stamp
%4 = keypress
%5 = target jump
%6 = position x
%7 = position y

t = nan(2000,1);
keypress = nan(2000,1);
reloc = nan(2000,1);
curr_pos = nan(2000,2);
curr_pos(1,:) = [0 0];

a = 0;
flipN = 1;

% get initial response
keyIsDown = 0;
keys = [];
while 1
    [keyIsDown, t0a, keyCode] = KbCheck;
    if keyIsDown && ismember(find(keyCode, 1, 'first'), params.keys)
        key = find(params.keys == find(keyCode,1,'first'));
        if isempty(find(data.choiceReloc(block, trial,:) == key)) == 0
            if GetEyes
                Eyelink('Message', 'respInit');
            end
            rt_init = (t0a - t_RT_onset)*1000;
            break
        else
            data.rt_init_err(block, trial) = (t0a - t_RT_onset)*1000;
            data.respAccInit(block, trial) = 0;
        end
    elseif keyIsDown && ismember(find(keyCode, 1, 'first'), params.exitkey) %interrupt experiment when esc key was hit
        sca;
        disp('Run terminated early by experimenter.')
        break
    elseif keyIsDown && ismember(find(keyCode, 1, 'first'), params.contkey) %jump to next block
        cont = 1;
        break
    end
end

while ~cont && (data.miss(block, trial) ~= 2 && (isempty(find(~isnan(curr_pos))) || ((isempty(find(~isnan(curr_pos(:,1))))==0) && (abs(curr_pos(find(~isnan(curr_pos(:,1)), 1, 'last'),1))-4 < params.target_offset(data.targ_arr(block, trial),1) && abs(curr_pos(find(~isnan(curr_pos(:,2)), 1, 'last'),2))-4 < params.target_offset(data.targ_arr(block, trial),2)))))
    
    reloc(flipN) = 0;
    t(flipN) = GetSecs;
    
    %% measure keypresses
    if (keyIsDown && ismember(find(keyCode,1,'first'), params.keys))
        key = find(params.keys == find(keyCode,1,'first'));
        keypress(flipN) = key;
        %only react if initial response was directed towards one of the target locations
        if isempty(find(data.choiceReloc(block, trial,:) == keypress(flipN))) == 0
            if (length(find(reloc == 1)) == 0) && (isempty(find(data.choiceLoc(block, trial,:) == keypress(flipN))) == 0)
                data.initChc(block, trial) = data.options(block,trial,find(data.choiceLoc(block, trial,:) == keypress(flipN)));
                if data.initChc(block, trial) <= dsgn.n_stim
                    data.initChc_cat(block, trial) = 1; %face
                else
                    data.initChc_cat(block, trial) = 2; %house
                end
            end
            
            if (isempty(find(data.choiceReloc(block, trial,:) == keypress(find(keypress > 0,1,'last')))) == 0)
                
                switch keypress(flipN)
                    case 1
                        target=[-params.target_offset(data.targ_arr(block, trial),1), params.target_offset(data.targ_arr(block, trial),2)];
                    case 2
                        target=[params.target_offset(data.targ_arr(block, trial),1), params.target_offset(data.targ_arr(block, trial),2)];
                    case 3
                        target=[params.target_offset(data.targ_arr(block, trial),1), -params.target_offset(data.targ_arr(block, trial),2)];
                    case 4
                        target=[-params.target_offset(data.targ_arr(block, trial),1), -params.target_offset(data.targ_arr(block, trial),2)];
                end
                
                if flipN > 1 && reloc(flipN-1) == 1
                    curr_pos(flipN,:) = curr_pos(flipN-1,:);
                end
                norm_vector=(target-curr_pos(flipN,:))./norm(target-(curr_pos(flipN,:)));%(target-curr_pos)./norm(target-curr_pos);

            end
        else
            if (length(find(reloc == 1)) == 0)
                data.respAccInit(block, trial) = 0;
            else
                data.respAccFin(block, trial) = 0;
            end
        end
    else
        keypress(flipN) = 0;
    end
    
    if isempty(find(keypress > 0))==0 && flipN > 1 && isnan(data.initChc_cat(block, trial)) == 0
        
        %create vector from current position to target ->
        %normalize and then multiply with speed to calculate new position
        curr_pos(flipN+1,:) = curr_pos(flipN,:) + (dsgn.speed*norm_vector);
        Screen('DrawTexture', curWindow, fixation);
        Screen('DrawTexture', curWindow, stim(data.options(block,trial,1)), [], params.target_locations(data.choiceReloc(block, trial, 1),:,data.targ_arr(block, trial)));
        Screen('DrawTexture', curWindow, stim(data.options(block,trial,2)), [], params.target_locations(data.choiceReloc(block, trial, 2),:,data.targ_arr(block, trial)));
        Screen('DrawTexture', curWindow, man, [], [x0+curr_pos(flipN,1)-(params.size_man(1)/2) y0-curr_pos(flipN,2)-(params.size_man(2)/2) x0+curr_pos(flipN,1)+(params.size_man(1)/2) y0-curr_pos(flipN,2)+(params.size_man(2)/2)], [], [], 2, manCol);
        Screen('Flip', curWindow, 0);
        
        %% just before target relocation: Get initial choice
        if a == 0 && (sqrt(ceil(curr_pos(flipN,1)^2)+ceil(curr_pos(flipN,2)^2)) >= (dsgn.jump*params.target_offset(1, 3)))
            a = 1;
            if data.trial_cond(block, trial) < 3
                reloc(flipN) = 1;
                % relocate
                if data.trial_cond(block, trial) == 1 %diagonal
                    data.choiceReloc(block, trial, find(data.choiceLoc(block, trial, :)==1)) = 3;
                    data.choiceReloc(block, trial, find(data.choiceLoc(block, trial, :)==2)) = 4;
                    data.choiceReloc(block, trial, find(data.choiceLoc(block, trial, :)==3)) = 1;
                    data.choiceReloc(block, trial, find(data.choiceLoc(block, trial, :)==4)) = 2;
                elseif data.trial_cond(block, trial) == 2 %vertical
                    if ismember(1,data.choiceLoc(block, trial, :)) && ismember(2,data.choiceLoc(block, trial, :))
                        data.choiceReloc(block, trial, find(data.choiceLoc(block, trial, :)==1)) = 4;
                        data.choiceReloc(block, trial, find(data.choiceLoc(block, trial, :)==2)) = 3;
                    elseif ismember(2,data.choiceLoc(block, trial, :)) && ismember(3,data.choiceLoc(block, trial, :))
                        data.choiceReloc(block, trial, find(data.choiceLoc(block, trial, :)==2)) = 1;
                        data.choiceReloc(block, trial, find(data.choiceLoc(block, trial, :)==3)) = 4;
                    elseif ismember(3,data.choiceLoc(block, trial, :)) && ismember(4,data.choiceLoc(block, trial, :))
                        data.choiceReloc(block, trial, find(data.choiceLoc(block, trial, :)==3)) = 2;
                        data.choiceReloc(block, trial, find(data.choiceLoc(block, trial, :)==4)) = 1;
                    elseif ismember(4,data.choiceLoc(block, trial, :)) && ismember(1,data.choiceLoc(block, trial, :))
                        data.choiceReloc(block, trial, find(data.choiceLoc(block, trial, :)==4)) = 3;
                        data.choiceReloc(block, trial, find(data.choiceLoc(block, trial, :)==1)) = 2;
                    end
                end
                
                Screen('DrawTexture', curWindow, fixation);
                Screen('DrawTexture', curWindow, stim(data.options(block,trial,1)), [], params.target_locations(data.choiceReloc(block, trial, 1),:,data.targ_arr(block, trial)));
                Screen('DrawTexture', curWindow, stim(data.options(block,trial,2)), [], params.target_locations(data.choiceReloc(block, trial, 2),:,data.targ_arr(block, trial)));
                Screen('DrawTexture', curWindow, man, [], [x0+curr_pos(flipN,1)-(params.size_man(1)/2) y0-curr_pos(flipN,2)-(params.size_man(2)/2) x0+curr_pos(flipN,1)+(params.size_man(1)/2) y0-curr_pos(flipN,2)+(params.size_man(2)/2)], [], [], 2, manCol); %params.manColour
                [~, ~, t_reloc] = Screen('Flip', curWindow, 0);
                if GetEyes
                    Eyelink('Message', 'targReloc');
                end
                while GetSecs-t_reloc < timing.delayCoM(block, trial)/1000
                    [keyIsDown,~,keyCode] = KbCheck;
                    if keyIsDown && ismember(find(keyCode,1,'first'), params.keys)
                        data.miss(block, trial) = -find(params.keys == find(keyCode,1,'first'));
                    end
                end
                manCol = params.manColour(2,:);
                Screen('DrawTexture', curWindow, fixation);
                Screen('DrawTexture', curWindow, man, [], [x0+curr_pos(flipN,1)-floor(params.size_man(1)/2) y0-curr_pos(flipN,2)-floor(params.size_man(2)/2) x0+curr_pos(flipN,1)+ceil(params.size_man(1)/2) y0-curr_pos(flipN,2)+ceil(params.size_man(2)/2)], [], [], 2, manCol);
                Screen('DrawTexture', curWindow, stim(data.options(block,trial,1)), [], params.target_locations(data.choiceReloc(block, trial, 1),:,data.targ_arr(block, trial)));
                Screen('DrawTexture', curWindow, stim(data.options(block,trial,2)), [], params.target_locations(data.choiceReloc(block, trial, 2),:,data.targ_arr(block, trial)));
                [~, ~, t_RT_reloc] = Screen('Flip', curWindow, 0);
                if GetEyes
                    Eyelink('Message', 'CoMgo');
                end
                timing.CoMgo(block, trial) = 1000*(t_RT_reloc - run_start);
                KbWait([],1);
                while 1
                    [keyIsDown,t0a,keyCode] = KbCheck;
                    if keyIsDown && ismember(find(keyCode,1,'first'), params.keys)
                        key = find(params.keys == find(keyCode,1,'first'));
                        keypress(flipN+1) = key;
                        if (isempty(find(data.choiceReloc(block, trial,:) == keypress(flipN+1))) == 0)
                            if GetEyes
                                Eyelink('Message', 'respReloc');
                            end
                            rt_reloc = (t0a - t_RT_reloc)*1000;
                            break
                        else
                            data.rt_reloc_err(block, trial) = (t0a - t_RT_reloc)*1000;
                            data.respAccFin(block, trial) = 0;
                        end
                    end
                end
            end
            
            %get initial target choice
            if curr_pos(flipN,1) < 0 && curr_pos(flipN,2) > 0 %upper left target
                init_targ = 1;
            elseif curr_pos(flipN,1) > 0 && curr_pos(flipN,2) > 0 %upper right target
                init_targ = 2;
            elseif curr_pos(flipN,1) > 0 && curr_pos(flipN,2) < 0 %lower right target
                init_targ = 3;
            elseif curr_pos(flipN,1) < 0 && curr_pos(flipN,2) < 0 %lower left target
                init_targ = 4;
            end
        end
    else
        curr_pos(flipN+1,:) = [0 0];
    end
    
    if isnan(keypress(flipN+1))
        [keyIsDown,t0a,keyCode] = KbCheck;
    end
    flipN = flipN + 1;
    
end

if ~cont
    mt = (GetSecs - t_RT_onset)*1000;
    curr_pos = curr_pos(~isnan(curr_pos(:,1)),:);
    trajectories = [repmat(trial,flipN-1,1) [1:flipN-1]' [0; diff(t(~isnan(t)))] keypress(~isnan(keypress)) reloc(~isnan(reloc)) curr_pos(1:end-1,:)];
    save(['data\trajectories\trajectories' num2str(data.id) '_' num2str(block) '_' num2str(trial) '.mat'], 'trajectories');
    data.nKey(block, trial) = length(find(diff(trajectories(find(trajectories(:,4)),4))))+1; %doesn't capture if pressed same key twice -> check in trajectory output! NOR when pressed during delayCoM
end

end

