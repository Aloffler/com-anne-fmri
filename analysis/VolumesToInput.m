function [AnalyseNii1, AnalyseNii2, AnalyseNii3, AnalyseNii4, AnalyseNii5] = VolumesToInput
% This function determines the volumes to input (nii) for each
% preprocessing step and needs to be modified based on your specific design

AnalyseNii1 = [];
AnalyseNii2 = [];
AnalyseNii3 = [];
AnalyseNii4 = [];
AnalyseNii5 = [];


global DESIGN ID MODEL
% Determine nr of volumes
nVol = DESIGN.nVol(ID,:);

%% ANALYSE

% 1st Run
for i = 1:nVol{1}
    AnalyseNii1{i,1} = [DESIGN.Paths.preprocData MODEL.model.glm_images DESIGN.Runs{1} ',' num2str(i)];
end

% 2nd Run
% a = length(AnalyseNii);
for i = 1:nVol{2}
    AnalyseNii2{i,1} = [DESIGN.Paths.preprocData MODEL.model.glm_images DESIGN.Runs{2} ',' num2str(i)];
end

% 3rd Run
% a = length(AnalyseNii);
for i = 1:nVol{3}
    AnalyseNii3{i,1} = [DESIGN.Paths.preprocData MODEL.model.glm_images DESIGN.Runs{3} ',' num2str(i)];
end

% 4th Run
% a = length(AnalyseNii);
for i = 1:nVol{4}
    AnalyseNii4{i,1} = [DESIGN.Paths.preprocData  MODEL.model.glm_images DESIGN.Runs{4} ',' num2str(i)];
end

% 5th Run
% a = length(AnalyseNii);
for i = 1:nVol{5}
    AnalyseNii5{i,1} = [DESIGN.Paths.preprocData  MODEL.model.glm_images DESIGN.Runs{5} ',' num2str(i)];
end
end