function crossClassROI

close all
clc

addpath('D:\TOOLBOXES\');


global ID DESIGN ROI
cd 'D:\Anne\1_preprocessing'

display('%%%%%%%%%%%%%%%%%%%%%%% LOADING PREPROCESSING SET UP - SPM12 %%%%%%%%%%%%%%%%%%%%%');display(' ');
files = struct2cell(dir); files = any(ismember(files(1,:),'PreprocessingCfg.mat'));
if files == 0      % if not: run the next function to set up the general configurations
    Preprocessing_cfg;
elseif files == 1   % if: load configurations (steps, params, design)
    display('%%%% STEP 1: Define experimental design parameters.'); display(' '); WaitSecs(1.5);
    display('Configuration file found. Step 2, Step 3 will be skipped and preprocessing will start shortly ...'); display(' ');
    load('PreprocessingCfg.mat');
end

DESIGN.Paths.analysis = ['D:\Anne\2_analysis\'];
DESIGN.Paths.analysisJobs = [DESIGN.Paths.analysis 'jobs'];

DESIGN.Paths.MVPA = 'D:\Anne\2_analysis\2_multivariate';
DESIGN.Paths.firstLevel = 'D:\Anne\2_analysis\2_multivariate\1_1st-Level\';

cd 'D:\Anne\2_analysis\ROIs'

ROI.ImgType = '.img';%'.nii'; %ROI images in nii or img format?

ROI.name = {
    'wMNI_Fusiform_R_roi',...
    'wMNI_Fusiform_RL_roi',...
    'wMNI_ParaHippocampal_R_roi',...
    'wMNI_ParaHippocampal_RL_roi',...
    'wMNI_Occipital_Mid_RL_roi',...
    'wMNI_Occipital_Inf_RL_roi',...
    'wMNI_Precentral_RL_roi',...
    'wMNI_Angular_RL_roi',...
    'wMNI_Angular_R_roi',...
    'wMNI_Angular_L_roi',...
    'wMNI_Precuneus_RL_roi',...
    'wMNI_Precuneus_RL_DORSAL_roi',...
    'wMNI_Precuneus_RL_VENTRAL_roi',...
    'wMNI_Precuneus_R_roi',...
    'wMNI_Precuneus_R_DORSAL_roi',...
    'wMNI_Precuneus_R_VENTRAL_roi',...
    'wMNI_Precuneus_L_roi',...
    'wMNI_Precuneus_L_DORSAL_roi',...
    'wMNI_Precuneus_L_VENTRAL_roi',...
    'wMNI_Cingulum_Post_RL_roi',...
    'wMNI_Cingulum_Post_R_roi',...
    'wMNI_Cingulum_Post_L_roi',...
    'wMNI_Cingulum_Ant_RL_roi',...
    'wMNI_Cingulum_Ant_R_roi',...
    'wMNI_Cingulum_Ant_L_roi',...
    'wMNI_Frontal_Sup_Medial_RL_roi',...
    'wMNI_Frontal_Sup_Medial_RL_DORSAL_roi',...
    'wMNI_Frontal_Sup_Medial_RL_VENTRAL_roi',...
    'wMNI_Frontal_Sup_Medial_R_roi',...
    'wMNI_Frontal_Sup_Medial_R_DORSAL_roi',...
    'wMNI_Frontal_Sup_Medial_R_VENTRAL_roi',...
    'wMNI_Frontal_Sup_Medial_L_roi',...
    'wMNI_Frontal_Sup_Medial_L_DORSAL_roi',...
    'wMNI_Frontal_Sup_Medial_L_VENTRAL_roi',...
    'wMNI_Frontal_Med_Orb_RL_roi',...
    'wMNI_Frontal_Med_Orb_R_roi',...
    'wMNI_Frontal_Med_Orb_L_roi',...
    'wMNI_Frontal_Mid_RL_roi',...
    'wMNI_Frontal_Mid_R_roi',...
    'wMNI_Frontal_Mid_L_roi',...
    'wMNI_Frontal_Mid_RL_DORSAL_roi',...
    'wMNI_Frontal_Mid_RL_VENTRAL_roi',...
    'wMNI_Frontal_Mid_R_DORSAL_roi',...
    'wMNI_Frontal_Mid_R_VENTRAL_roi',...
    'wMNI_Frontal_Mid_L_DORSAL_roi',...
    'wMNI_Frontal_Mid_L_VENTRAL_roi',...
    };

ROI.dir = 'D:\Anne\2_analysis\ROIs\ROIimg\';

cd(DESIGN.Paths.MVPA)

% select models for crossclassification
modelnames = {
    {'14_initTargRespBin', '15_finTargRespBin', '1_TargLocBin_cross'};
    {'7_initFace_House_choiceOn', '24_finFace_House_exclErrEarly', '2b_fhChoice_crossExcl'};
    {'7_initFace_House_choiceOn', '18_finFace_House_noCoM', '2c_fhChoice_noCoM'};
    {'7_initFace_House_choiceOn', '17_finFace_House_CoM', '2d_fhChoice_CoM'};
    {'7_initFace_House_choiceOn', '20_finFace_House_noCoM_excluded', '2e_fhChoice_noCoM_excluded'};
    {'7_initFace_House_choiceOn', '19_finFace_House_CoM_excluded', '2f_fhChoice_CoM_excluded'};
    {'3_initDist_Close_Far', '25_targDist_fin_diag', '3a_TargDist_cross_diag'};
    {'3_initDist_Close_Far', '26_targDist_fin_exclErrEarly', '3b_TargDist_cross_Excl'};
    {'7_initFace_House_choiceOn', '8_initFace_House_targOn', '21_fhChoice_ChOnTargOn'};
    };



for dur = 1%:2
    
    switch dur
        case 1
            DESIGN.duration = '1_event';
        case 2
            DESIGN.duration = '2_period';
    end
    
    for i = 1:length(DESIGN.Participants)
        ID = DESIGN.Participants(i);
        
        if ID ~= 5 && ID ~= 8 && ID~= 23
            
            for model_nr = 1:length(modelnames)
                display('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
                display(['%%%%%% START DECODING, MODEL ' num2str(model_nr) ': ' modelnames{model_nr}{3} ', ID: ' num2str(ID) '%%%%%%%%%% ' DESIGN.duration]); display(' ');
                
                beta_dir1 = [DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr}{1} '\ID' num2str(ID) '\'];
                beta_dir2 = [DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr}{2} '\ID' num2str(ID) '\'];
                
                for r = 1:length(ROI.name)
                    DESIGN.output_dir = [DESIGN.Paths.MVPA '\2b_MVPAroiX\' DESIGN.duration '\' modelnames{model_nr}{3} '\' ROI.name{r} '\ID' num2str(ID) '\'];
                    if ~exist(DESIGN.output_dir) && exist([DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr}{1} '\ID' num2str(ID) '\']) && exist([DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr}{2} '\ID' num2str(ID) '\'])
                        
                        mkdir(DESIGN.output_dir)
                        
                        
                        %% configure decoding
                        cfg = decoding_defaults();
                        
                        % find names of conditions by excluding motion regressors
                        % (set 1)
                        regressor_names1 = design_from_spm(beta_dir1);
                        labels1 = regressor_names1(1,1:6);
                        labels1 = labels1(find(ismember(labels1, {'X', 'Y', 'Z', 'x', 'y', 'z'}) == 0));
                        %                 labels1 = strcat(labels1, '1');
                        
                        % find names of conditions by excluding motion regressors
                        % (set 2)
                        regressor_names2 = design_from_spm(beta_dir2);
                        labels2 = regressor_names2(1,1:6);
                        labels2 = labels2(find(ismember(labels2, {'X', 'Y', 'Z', 'x', 'y', 'z'}) == 0));
                        %                 labels2 = strcat(labels2, '2');
                        
                        
                        if length(labels1) == 2
                            cfg = decoding_describe_data(cfg,[labels1, labels2],[1 -1 1 -1], regressor_names2, beta_dir2, [1 1 2 2]);
                        elseif length(labels1) == 4 %model with 4 response alternatives
                            cfg = decoding_describe_data(cfg,[labels1, labels2],[1 2 3 4 1 2 3 4], regressor_names2, beta_dir2, [1 1 1 1 2 2 2 2]);
                        end
                        
                        
                        %% find out if same number of runs for set and 2, if not, use min number of runs available for both
                        folder = [DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr}{1} '\ID' num2str(ID) '\'];
                        nfiles1 = dir([folder 'beta_*']);
                        nfiles1 = length({nfiles1.name}');
                        
                        folder = [DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr}{2} '\ID' num2str(ID) '\'];
                        nfiles2 = dir([folder 'beta_*']);
                        nfiles2 = length({nfiles2.name}');
                        
                        [minN, minInd] = min([nfiles1 nfiles2]);
                        
                        get_betas = {};
                        n_regressors = length(labels1) + 6; % 2 (e.g., left vs. right) + 6 motion regressors
                        
                        doRuns = minN/(n_regressors + 1); % +1 constant
                        
                        display(['nRuns = '  num2str(doRuns) '; beta img 1 = ' num2str(nfiles1) '; beta img 2 = ' num2str(nfiles2)]);
                        
                        if doRuns > 2
                            b = 1;
                            for set = 1:2
                                folder = [DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr}{set} '\ID' num2str(ID) '\'];
                                files1 = dir([folder 'beta_*']);
                                filenames1 = {files1.name}';
                                
                                for class = 1:length(labels1)
                                    run = 0;
                                    while run < 5
                                        run = run + 1;
                                        if DESIGN.nVol{ID,run} > 0
                                            if (class+(run-1)*n_regressors) <= (minN - doRuns) %1 beta image for constant per run -> don't use for decoding
                                                get_betas(b,1) = strcat(folder, filenames1(class+(run-1)*n_regressors));
                                                b=b+1;
                                            end
                                        end
                                    end
                                end
                            end
                            
                            cfg.files.name = get_betas;
                            
                            cfg.design = make_design_xclass_cv(cfg); %create design
                            
                            cfg.analysis ='roi'; %wholebrain, ROI
                            
                            if r == 1
                                cfg.plot_design = 1;
                            else
                                cfg.plot_design = 0;
                            end
                            
                            %% mask ROI
                            ROI.img_file = [ROI.dir 'ID' num2str(ID) '\' ROI.name{r} ROI.ImgType];
                            DESIGN.beta_dir = beta_dir1;
                            
                            nrun = 1; % enter the number of runs here
                            jobfile = {'D:\Anne\2_analysis\jobs\maskROI_job.m'};
                            jobs = repmat(jobfile, 1, nrun);
                            inputs = cell(0, nrun);
                            for crun = 1:nrun
                            end
                            spm('defaults', 'FMRI');
                            spm_jobman('run', jobs, inputs{:});
                            
                            %% binarise ROI to exclude areas with high uncertainty
                            cd(DESIGN.output_dir);
                            fname='maskedROI';
                            ROI_hdr=spm_vol([fname '.img']);
                            ROI_vol=spm_read_vols(ROI_hdr);
                            ROI_hdr.pinfo = [1 1 1]';
                            
                            
                            ROI_hdr.fname=[DESIGN.output_dir 'maskedROI_bin.img'];
                            spm_write_vol(ROI_hdr,ROI_vol);
                            
                            
                            cfg.files.mask = {[DESIGN.output_dir  'maskedROI_bin.img']};%{[beta_dir 'mask.nii']};
                            
                            %design_check = decoding_load_data(cfg)
                            
                            
                            %% or classification_kernel?
                            cfg.decoding.method = 'classification';
                            %'model_parameters'
                            if length(labels1) == 2 %binary classification
                                cfg.results.output ={'accuracy_minus_chance'; 'AUC_minus_chance'; 'sensitivity_minus_chance'; 'specificity_minus_chance'; 'balanced_accuracy_minus_chance'; 'signed_decision_values'}; %confusion_matrix
                            else %multiclass classification
                                cfg.results.output ={'accuracy_minus_chance'; 'balanced_accuracy_minus_chance'};
                            end
                            
                            cfg.results.dir = DESIGN.output_dir;
                            cfg.results.overwrite = 1;
                            
                            % %% Scale data
                            cfg.scale.method ='z'; %z-normalised, alternatively min0-max1
                            cfg.scale.estimation ='across'; %scaling estimated on training data and applied to test data
                            cfg.scale.cutoff =[-3 3];%outliers = +/- 3SD
                            cfg.scale.check_datatrans_ok = true;
                            
                            %% set C to 1 % parameters for libsvm (linear SV classification, cost = 1)
                            cfg.decoding.train.classification.model_parameters = '-s 0 -t 0 -c 1 -b 0 -q'; %= default
                            
                            cfg.verbose = 1; %(0: no output, 1: normal output [default], 2: high output).
                            cfg.plot_selected_voxels = 1;%10000; %plot searchlight of every 10000th step (default = 0 -> no plotting)
                            
                            %% DECODING
                            results = decoding(cfg);
                            
                            %% PERMUTATION test
                            cfg.results.setwise = 1;
                            cfg.design = make_design_permutation(cfg,length(labels1)^(length(cfg.design.set))/2,1); %16 permutations
                            
                            cfg.stats.test = 'permutation'; %set test
                            cfg.stats.tail = 'right';
                            cfg.stats.output = {'accuracy_minus_chance'};
                            
                            [reference,cfg] = decoding(cfg);
                            
                            
                            %% plot results and save figure
                            %                         figure(99)
                            %                         %set(gcf, 'Position', get(0, 'Screensize'));
                            %                         subplot(1,5,1);bar(results.accuracy_minus_chance.output); ylim([-50, 50]); title('ACC above chance');
                            %                         subplot(1,5,2);bar(results.balanced_accuracy_minus_chance.output); ylim([-50, 50]); title('ACC (balanced)');
                            %                         if length(labels1) == 2 %binary classification
                            %                             subplot(1,5,3);bar(results.AUC_minus_chance.output); ylim([-50, 50]); title('AUC above chance');
                            %                             subplot(1,5,4);bar(results.sensitivity_minus_chance.output); ylim([-50, 50]); title('Sensitivity');
                            %                             subplot(1,5,5);bar(results.specificity_minus_chance.output); ylim([-50, 50]); title('Specificity');
                            %                         end
                            %
                            %                         saveas(figure(99),[DESIGN.output_dir 'results.jpg'])
                            %                             close all
                        else
                            rmdir(DESIGN.output_dir)
                        end
                    end
                end
            end
        end
    end
end

close all


