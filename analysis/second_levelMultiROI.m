clear all
close all
clc

basedir = 'C:\Users\action\Desktop\Anne\2_analysis\';

chance_level = 50;
output_type = 'accuracy_minus_chance' %'AUC_minus_chance'; 'sensitivity_minus_chance'; 'specificity_minus_chance'; 'signed_decision_values'}

cross_class = 0;

dur = '1_event\';%'2_period\';%
model = '15_finTargRespBin';
% '3_initDist_Close_Far'
% '26_targDist_fin_exclErrEarly'
% '25_targDist_fin_diag'


% '7_initFace_House_choiceOn'
% '8_initFace_House_targOn'
% '24_finFace_House_exclErrEarly'
% '22_finFace_HouseDiag'
% '17_finFace_House_CoM'
% '18_finFace_House_noCoM'

% '21_noCoM_CoM_Reloc_exclEarlyError'
% '11_noCoM_CoM_Reloc'
% '16_noCoM_CoM_Reloc_exclEarly'
% '27_noCoM_CoM_choiceOn_exclEarlyError'

% '14_initTargRespBin'
% '15_finTargRespBin'

% '23_initFace_House_choiceOn_delP'

% '1_TargLocBin_cross'
% '2b_fhChoice_crossExcl'
% '2_fhChoice_cross'
% '2a_fhChoice_crossDiag'
% '2c_fhChoice_noCoM'
% '2d_fhChoice_CoM'
% '2e_fhChoice_noCoM_excluded'
% '2f_fhChoice_CoM_excluded'
% '3b_TargDist_cross_Excl'
% '3_TargDist_cross'
% '3a_TargDist_cross_diag'
% '4_choice_cross_TargOn1'
% '4a_choice_cross_TargOn2'
% '21_fhChoice_ChOnTargOn'

ROI.name = {'wMNI_Frontal_Mid_RL_roi'};

% ROI.name = {
%     'wMNI_Fusiform_R_roi',...
%     'wMNI_Fusiform_RL_roi',...
%     'wMNI_ParaHippocampal_R_roi',...
%     'wMNI_ParaHippocampal_RL_roi',...
%     'wMNI_Occipital_Mid_RL_roi',...
%     'wMNI_Occipital_Inf_RL_roi',...
%     'wMNI_Precentral_RL_roi',...
%     'wMNI_Angular_RL_roi',...
%     'wMNI_Angular_R_roi',...
%     'wMNI_Angular_L_roi',...
%     'wMNI_Precuneus_RL_roi',...
%     'wMNI_Precuneus_RL_DORSAL_roi',...
%     'wMNI_Precuneus_RL_VENTRAL_roi',...
%     'wMNI_Precuneus_R_roi',...
%     'wMNI_Precuneus_R_DORSAL_roi',...
%     'wMNI_Precuneus_R_VENTRAL_roi',...
%     'wMNI_Precuneus_L_roi',...
%     'wMNI_Precuneus_L_DORSAL_roi',...
%     'wMNI_Precuneus_L_VENTRAL_roi',...
%     'wMNI_Cingulum_Post_RL_roi',...
%     'wMNI_Cingulum_Post_R_roi',...
%     'wMNI_Cingulum_Post_L_roi',...
%     'wMNI_Cingulum_Ant_RL_roi',...
%     'wMNI_Cingulum_Ant_R_roi',...
%     'wMNI_Cingulum_Ant_L_roi',...
%     'wMNI_Frontal_Sup_Medial_RL_roi',...
%     'wMNI_Frontal_Sup_Medial_RL_DORSAL_roi',...
%     'wMNI_Frontal_Sup_Medial_RL_VENTRAL_roi',...
%     'wMNI_Frontal_Sup_Medial_R_roi',...
%     'wMNI_Frontal_Sup_Medial_R_DORSAL_roi',...
%     'wMNI_Frontal_Sup_Medial_R_VENTRAL_roi',...
%     'wMNI_Frontal_Sup_Medial_L_roi',...
%     'wMNI_Frontal_Sup_Medial_L_DORSAL_roi',...
%     'wMNI_Frontal_Sup_Medial_L_VENTRAL_roi',...
%     'wMNI_Frontal_Med_Orb_RL_roi',...
%     'wMNI_Frontal_Med_Orb_R_roi',...
%     'wMNI_Frontal_Med_Orb_L_roi',...
%     'wMNI_Frontal_Mid_RL_roi',...
%     'wMNI_Frontal_Mid_RL_DORSAL_roi',...
%     'wMNI_Frontal_Mid_RL_VENTRAL_roi',...
%     'wMNI_Frontal_Mid_R_roi',...
%     'wMNI_Frontal_Mid_R_DORSAL_roi',...
%     'wMNI_Frontal_Mid_R_VENTRAL_roi',...
%     'wMNI_Frontal_Mid_L_roi',...
%     'wMNI_Frontal_Mid_L_DORSAL_roi',...
%     'wMNI_Frontal_Mid_L_VENTRAL_roi',...
%     };

display('   ');
display(['%%%%%%%%% Model: ' model ' %%%%%%%%%']);

for r = 1:length(ROI.name)
    
    if cross_class
        DESIGN.output_dir = [basedir '2_multivariate\2b_MVPAroiX\' dur model '\' ROI.name{r}];
    else
        DESIGN.output_dir = [basedir '2_multivariate\2b_MVPAroi\' dur model '\' ROI.name{r}];
    end
    
    IDs = dir(DESIGN.output_dir);
    IDs = IDs([IDs.isdir]);
    IDs = {IDs.name};
    IDs = IDs(3:end);
    IDs = sort_nat(IDs);
    
    resultsAll = [];
    resultsAll_perm = nan(length(IDs),16);%max of 16 permutations, for some models/IDs, only 8 -> set remaining cells to nan
    
    %% get results for all IDs
    for i = 1:length(IDs)
        load([DESIGN.output_dir '\' IDs{i} '\res_' output_type '.mat']);
        if length(find(output_type(1:3)=='acc'))==3
            resultsAll(i) = results.accuracy_minus_chance.output+50;
        elseif length(find(output_type(1:3)=='AUC'))==3
            resultsAll(i) = results.AUC_minus_chance.output+50;
        elseif length(find(output_type(1:3)=='sen'))==3
            resultsAll(i) = results.sensitivity_minus_chance.output+50;
        elseif length(find(output_type(1:3)=='spe'))==3
            resultsAll(i) = results.specificity_minus_chance.output+50;
            
        end
        
        
        
        nVox(r,i) = length(results.mask_index_each{1,1}); %get number of voxels for each ID
        
        for p = 1:length(dir([DESIGN.output_dir '\' IDs{i} '\res_' output_type '_set*']))
            if p < 10
                load([DESIGN.output_dir '\' IDs{i} '\res_' output_type '_set000' num2str(p) '.mat']);
            else
                load([DESIGN.output_dir '\' IDs{i} '\res_' output_type '_set00' num2str(p) '.mat']);
            end          
            
            if length(find(output_type(1:3)=='acc'))==3
                resultsAll_perm(i, p) = results.accuracy_minus_chance.output+50;
            elseif length(find(output_type(1:3)=='AUC'))==3
                resultsAll_perm(i, p) = results.AUC_minus_chance.output+50;
            elseif length(find(output_type(1:3)=='sen'))==3
                resultsAll_perm(i, p) = results.sensitivity_minus_chance.output+50;
            elseif length(find(output_type(1:3)=='spe'))==3
                resultsAll_perm(i, p) = results.specificity_minus_chance.output+50;
            end
                       
        end
    end
    
    resultsAll_perm_mean = nanmean(resultsAll_perm(:,2:end),2);
    
    %% save accuracy for all ROIs
    resultsROI(:,r) = resultsAll(:);
   
    %% compute statistics
    %     resultsAll(:)
    [h, p, ci, stats] = ttest(resultsAll(:), chance_level, [],'right');
    [h_p, p_p, ci_p, stats_p] = ttest(resultsAll(:), resultsAll_perm_mean(:), [],'right');
    [h_pc, p_pc, ci_pc, stats_pc] = ttest(resultsAll_perm_mean(:), chance_level, [],'right');
    
    
    %% plot mean and CIs
    figure(r);
    set(gca,'position',[.2 .2 .6 .6],'units','normalized')
    title({['Model: ' model], ''}, 'FontWeight', 'bold', 'Interpreter', 'none'); hold on;
    bar([1],[mean(resultsAll)], 'FaceColor', [.6 .6 .6]); ylim([0 100]); xlim([0 3]); %hold on
    %     bar([1 2],[mean(resultsAll) mean(resultsAll_perm_mean)], 'FaceColor', [.6 .6 .6]); ylim([0 100]); xlim([0 3]); %hold on
    errorbar(1,mean(resultsAll), mean(resultsAll)-ci(1), 'Color',[0 0 0]); %hold on
    %     errorbar(2,mean(resultsAll_perm_mean),mean(resultsAll_perm_mean)-ci_pc(1), 'Color',[0 0 0]); %hold on
    plot(0:.1:3, repmat(50, length([0:.1:3]),1), '-.', 'Color', [0 0 0])
    %     if h
    %         plot(1, 90, '*', 'Color', [1 0 0]);
    %         display([ROI.name{r} ', ACC = ' num2str(mean(resultsAll)) ' (p = ' num2str(p) ')']);
    %     end
    
    if p_p < .05/6
        display([ROI.name{r} ', ACC = ' num2str(mean(resultsAll)) ' (p = ' num2str(p_p) ')']);
    elseif p_p < .05
        display(['MARGINAL??? ' ROI.name{r} ', ACC = ' num2str(mean(resultsAll)) ' (p = ' num2str(p_p) ')']);
    end
    
    if p_p < .001/6
        text(0.88, 90, '***', 'Color', [0 0 0],'FontWeight', 'bold', 'FontSize',16);
    elseif p_p < .01/6
        text(0.92, 90, '**', 'Color', [0 0 0],'FontWeight', 'bold', 'FontSize',16);
    elseif p_p < .05/6
        text(0.96, 90, '*', 'Color', [0 0 0],'FontWeight', 'bold', 'FontSize',16);
    end
    set(gca,'xtick',[1,2],'XtickLabel',['','']);%{'ROI', 'Permutation'});
    %     set(gca,'YtickLabel',['10','20','30','40','50','60','70','80','90','100'],'FontSize',14);
    set(gca,'YTickLabel', {[0,20,40,60,80,100]}, 'fontsize', 16);
    
    set(gcf,'Color','w');
    
    ylabel('Decoding Accuracy (%)', 'FontWeight', 'bold', 'Interpreter', 'none','FontSize',16);
    %     xlabel({''; ['ROI: ' ROI.name{r}]; ['ACC = ' num2str(mean(resultsAll)) ' (N = ' num2str(length(IDs)) ')']}, 'FontWeight', 'bold', 'Interpreter', 'none','FontSize',14);
    text(0.86, 30, sprintfc('%0.1f',mean(resultsAll)),'FontSize',14);
    
    %         export_fig([DESIGN.output_dir '\results'  output_type(1:3) '.tif'], '-r864')
    
    %     KbWait
    %     close all
    
end

% display('   ');
% for roi = 1:r
%     display([ROI.name{roi} ' number of voxels: M = ' num2str(mean(nVox(roi,:))) ' SD = ' num2str(std(nVox(roi,:))) ' Min = ' num2str(min(nVox(roi,:))) ' Max = ' num2str(max(nVox(roi,:)))]);
% end

length(IDs)
save('resultsROI.mat','resultsROI');

% resultsAll'