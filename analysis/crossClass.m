function crossClass

close all
clear
clc

global DESIGN
cd 'D:\Anne\1_preprocessing'

display('%%%%%%%%%%%%%%%%%%%%%%% LOADING PREPROCESSING SET UP - SPM12 %%%%%%%%%%%%%%%%%%%%%');display(' ');
files = struct2cell(dir); files = any(ismember(files(1,:),'PreprocessingCfg.mat'));
if files == 0      % if not: run the next function to set up the general configurations
    Preprocessing_cfg;
elseif files == 1   % if: load configurations (steps, params, design)
    display('%%%% STEP 1: Define experimental design parameters.'); display(' '); WaitSecs(1.5);
    display('Configuration file found. Step 2, Step 3 will be skipped and preprocessing will start shortly ...'); display(' ');
    load('PreprocessingCfg.mat');
end

DESIGN.Paths.analysis = ['D:\Anne\2_analysis\'];
DESIGN.Paths.analysisJobs = [DESIGN.Paths.analysis 'jobs'];

DESIGN.Paths.MVPA = 'D:\Anne\2_analysis\2_multivariate';
DESIGN.Paths.firstLevel = 'D:\Anne\2_analysis\2_multivariate\1_1st-Level\';

cd(DESIGN.Paths.MVPA)

t0 = GetSecs;

% select models for crossclassification
modelnames = {
    {'14_initTargRespBin', '15_finTargRespBin', '1_TargResp_cross'};
    {'7_initFace_House_choiceOn', '9_finFace_House', '2_fhChoice_cross'};
    {'3_initDist_Close_Far', '4_finDist_Close_Far', '3_TargDist_cross'}
    {'7_initFace_House_choiceOn', '22_finFace_HouseDiag', '2a_fhChoice_crossDiag'}};

for dur = 1:2
    
    switch dur
        case 1
            DESIGN.duration = '1_event';
        case 2
            DESIGN.duration = '2_period';
    end
    
    for i = 1:length(DESIGN.Participants)
        ID = DESIGN.Participants(i);
        
        for model_nr = 1:length(modelnames)
            display('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
            display(['%%%%%% START DECODING, MODEL ' num2str(model_nr) ': ' modelnames{model_nr}{3} ', ID: ' num2str(ID) '%%%%%%%%%% ' DESIGN.duration]); display(' ');
            
            beta_dir1 = [DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr}{1} '\ID' num2str(ID) '\'];
            beta_dir2 = [DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr}{2} '\ID' num2str(ID) '\'];
            
            output_dir = [DESIGN.Paths.MVPA '\2a_MVPAcross\' DESIGN.duration '\' modelnames{model_nr}{3} '\ID' num2str(ID) '\'];
            if ~exist(output_dir)
                mkdir(output_dir)
                
                
                %% configure decoding
                cfg = decoding_defaults();
                
                % find names of conditions by excluding motion regressors
                % (set 1)
                regressor_names1 = design_from_spm(beta_dir1);
                labels1 = regressor_names1(1,1:6);
                labels1 = labels1(find(ismember(labels1, {'X', 'Y', 'Z', 'x', 'y', 'z'}) == 0));
                %                 labels1 = strcat(labels1, '1');
                
                % find names of conditions by excluding motion regressors
                % (set 2)
                regressor_names2 = design_from_spm(beta_dir2);
                labels2 = regressor_names2(1,1:6);
                labels2 = labels2(find(ismember(labels2, {'X', 'Y', 'Z', 'x', 'y', 'z'}) == 0));
                %                 labels2 = strcat(labels2, '2');
                
                
                if length(labels1) == 2
                    cfg = decoding_describe_data(cfg,[labels1, labels2],[1 -1 1 -1], regressor_names1, beta_dir1, [1 1 2 2]);
                elseif length(labels1) == 4 %model with 4 response alternatives
                    cfg = decoding_describe_data(cfg,[labels1, labels2],[1 2 3 4 1 2 3 4], regressor_names1, beta_dir1, [1 1 1 1 2 2 2 2]);
                end
                
                
                %% find out if same number of runs for set and 2, if not, use min number of runs available for both
                folder = [DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr}{1} '\ID' num2str(ID) '\'];
                nfiles1 = dir([folder 'beta_*']);
                nfiles1 = length({nfiles1.name}');
                
                folder = [DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr}{2} '\ID' num2str(ID) '\'];
                nfiles2 = dir([folder 'beta_*']);
                nfiles2 = length({nfiles2.name}');
                
                minN = min(nfiles1, nfiles2);
                                
                get_betas = {};
                n_regressors = length(labels1) + 6; % 2 (e.g., left vs. right) + 6 motion regressors
                
                doRuns = nfiles1/(n_regressors + 1); % +1 constant
                
                display(['nRuns = '  num2str(doRuns) '; beta img 1 = ' num2str(nfiles1) '; beta img 2 = ' num2str(nfiles2)]);

                b = 1;
                for set = 1:2
                    folder = [DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr}{set} '\ID' num2str(ID) '\'];
                    files1 = dir([folder 'beta_*']);
                    filenames1 = {files1.name}';
                    for class = 1:length(labels1)
                        run = 0;
                        while run < 5
                            run = run + 1;
                            if DESIGN.nVol{ID,run} > 0
                                if (class+(run-1)*n_regressors) <= (length(filenames1) - doRuns) %1 beta image for constant per run -> don't use for decoding
                                    get_betas(b,1) = strcat(folder, filenames1(class+(run-1)*n_regressors));
                                    b=b+1;
                                end
                            end
                        end
                    end
                end
                
                cfg.files.name = get_betas;
                
                cfg.design = make_design_xclass_cv(cfg); %create design
                
                cfg.analysis ='searchlight'; %wholebrain, ROI
                cfg.searchlight.unit = 'voxels'; % comment or set to 'mm' if you want normal voxels
                cfg.searchlight.radius = 4;
                cfg.searchlight.spherical = 0;%0=default, set to 1???
                %             cfg.searchlight.subset = [repmat(kron([1:50]',ones(9,1)),1,1) repmat(kron([1:50]',ones(3,1)),3,1) repmat(kron([1:50]',ones(1,1)),9,1)]; % x/y/z coordinates of middle voxel (or provide single index)
                
                cfg.files.mask = {[beta_dir1 'mask.nii']};
                
                %% or classification_kernel?
                cfg.decoding.method = 'classification';
                %'model_parameters'
                if length(labels1) > 2
                    cfg.results.output ={'accuracy_minus_chance';'balanced_accuracy_minus_chance'}; %confusion_matrix
                else
                    cfg.results.output ={'accuracy_minus_chance'; 'AUC_minus_chance'; 'sensitivity_minus_chance'; 'specificity_minus_chance'; 'balanced_accuracy_minus_chance'}; %confusion_matrix
                end
                cfg.results.dir = output_dir;
                cfg.results.overwrite = 1;
                
                % %% Scale data??
                cfg.scale.method ='z'; %z-normalised, alternatively min0-max1
                cfg.scale.estimation ='across'; %scaling estimated on training data and applied to test data
                cfg.scale.cutoff =[-3 3];%outliers = +/- 3SD
                cfg.scale.check_datatrans_ok = true;
                
                %% set C to 1 % parameters for libsvm (linear SV classification, cost = 1)
                cfg.decoding.train.classification.model_parameters = '-s 0 -t 0 -c 1 -b 0 -q'; %= default
                
                %% Feature selection??
                % cfg.feature_selection.method = 'filter';
                % cfg.feature_selection.filter = 'F'; %'external';
                % % % cfg.feature_selection.external_fname = 'mylocalizer.img';
                % cfg.feature_selection.n_vox =0.5;%select 50% of all voxels, then run Recursive Feature Elimination on the remainingvoxels
                % cfg.feature_selection.check_datatrans_ok = true;
                
                cfg.verbose = 1; %(0: no output, 1: normal output [default], 2: high output).
                cfg.plot_selected_voxels = 0;%10000; %plot searchlight of every 10000th step (default = 0 -> no plotting)
                
                %% DECODING
                results = decoding(cfg);
                
                %                 %% PERMUTATION test
                %                 cfg.results.setwise = 1;
                %                 cfg.design = make_design_permutation(cfg,16,1); %16 permutations
                %                 [reference,cfg] = decoding(cfg);
                %                 cfg.stats.test = 'permutation'; %set test
                %                 cfg.stats.tail = 'right';
                %                 cfg.stats.output = {'accuracy_minus_chance'};
                %                 p = decoding_statistics(cfg,results,reference);
                %                 save([output_dir 'permutation', 'p']);
                
                close all
            end
        end
    end
end

display(['*** DURATION: ' num2str((GetSecs-t0)/60) ' minutes ***']);

close all


