function decodeROI

close all
clc

set(0, 'DefaultFigureVisible', 'off');

addpath('D:\TOOLBOXES');

global ID DESIGN ROI
cd 'D:\Anne\1_preprocessing'

display('%%%%%%%%%%%%%%%%%%%%%%% LOADING PREPROCESSING SET UP - SPM12 %%%%%%%%%%%%%%%%%%%%%');display(' ');
files = struct2cell(dir); files = any(ismember(files(1,:),'PreprocessingCfg.mat'));
if files == 0      % if not: run the next function to set up the general configurations
    Preprocessing_cfg;
elseif files == 1   % if: load configurations (steps, params, design)
    display('%%%% STEP 1: Define experimental design parameters.'); display(' '); WaitSecs(1.5);
    display('Configuration file found. Step 2, Step 3 will be skipped and preprocessing will start shortly ...'); display(' ');
    load('PreprocessingCfg.mat');
end

DESIGN.Paths.analysis = ['D:\Anne\2_analysis\'];
DESIGN.Paths.analysisJobs = [DESIGN.Paths.analysis 'jobs'];

DESIGN.Paths.MVPA = 'D:\Anne\2_analysis\2_multivariate';
DESIGN.Paths.firstLevel = 'D:\Anne\2_analysis\2_multivariate\1_1st-Level\';


cd('D:\Anne\2_analysis\ROIs')

ROI.ImgType = '.img'; %'.nii';% ROI images in nii or img format?
ROI.name = {
    'wMNI_Fusiform_R_roi',...
    'wMNI_Fusiform_RL_roi',...
    'wMNI_ParaHippocampal_R_roi',...
    'wMNI_ParaHippocampal_RL_roi',...
    'wMNI_Occipital_Mid_RL_roi',...
    'wMNI_Occipital_Inf_RL_roi',...
    'wMNI_Precentral_RL_roi',...
    'wMNI_Angular_RL_roi',...
    'wMNI_Angular_R_roi',...
    'wMNI_Angular_L_roi',...
    'wMNI_Precuneus_RL_roi',...
    'wMNI_Precuneus_RL_DORSAL_roi',...
    'wMNI_Precuneus_RL_VENTRAL_roi',...
    'wMNI_Precuneus_R_roi',...
    'wMNI_Precuneus_R_DORSAL_roi',...
    'wMNI_Precuneus_R_VENTRAL_roi',...
    'wMNI_Precuneus_L_roi',...
    'wMNI_Precuneus_L_DORSAL_roi',...
    'wMNI_Precuneus_L_VENTRAL_roi',...
    'wMNI_Cingulum_Post_RL_roi',...
    'wMNI_Cingulum_Post_R_roi',...
    'wMNI_Cingulum_Post_L_roi',...
    'wMNI_Cingulum_Ant_RL_roi',...
    'wMNI_Cingulum_Ant_R_roi',...
    'wMNI_Cingulum_Ant_L_roi',...
    'wMNI_Frontal_Sup_Medial_RL_roi',...
    'wMNI_Frontal_Sup_Medial_RL_DORSAL_roi',...
    'wMNI_Frontal_Sup_Medial_RL_VENTRAL_roi',...
    'wMNI_Frontal_Sup_Medial_R_roi',...
    'wMNI_Frontal_Sup_Medial_R_DORSAL_roi',...
    'wMNI_Frontal_Sup_Medial_R_VENTRAL_roi',...
    'wMNI_Frontal_Sup_Medial_L_roi',...
    'wMNI_Frontal_Sup_Medial_L_DORSAL_roi',...
    'wMNI_Frontal_Sup_Medial_L_VENTRAL_roi',...
    'wMNI_Frontal_Med_Orb_RL_roi',...
    'wMNI_Frontal_Med_Orb_R_roi',...
    'wMNI_Frontal_Med_Orb_L_roi',...
    'wMNI_Frontal_Mid_RL_roi',...
    'wMNI_Frontal_Mid_R_roi',...
    'wMNI_Frontal_Mid_L_roi',...
    'wMNI_Frontal_Mid_RL_DORSAL_roi',...
    'wMNI_Frontal_Mid_RL_VENTRAL_roi',...
    'wMNI_Frontal_Mid_R_DORSAL_roi',...
    'wMNI_Frontal_Mid_R_VENTRAL_roi',...
    'wMNI_Frontal_Mid_L_DORSAL_roi',...
    'wMNI_Frontal_Mid_L_VENTRAL_roi',...
    };

ROI.dir = 'D:\Anne\2_analysis\ROIs\ROIimg\';

cd(DESIGN.Paths.MVPA)


for dur = 1%:2
    
    switch dur
        case 1
            DESIGN.duration = '1_event';
        case 2
            DESIGN.duration = '2_period';
    end
    
    models = dir([DESIGN.Paths.firstLevel DESIGN.duration]);
    modelnames = {models.name}';
    modelnames = modelnames(3:end);
    modelnames = sort_nat(modelnames);
    
    for i = 1:length(DESIGN.Participants)
        ID = DESIGN.Participants(i);
        
        if ID ~= 5 && ID ~= 8 && ID~= 23
            for model_nr = 1:length(modelnames)
                
                DESIGN.beta_dir = [DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr} '\ID' num2str(ID) '\'];
                
                for r = 1:length(ROI.name) %24/27
                    if exist(DESIGN.beta_dir)
                        display('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
                        display(['%%%%%% START DECODING, MODEL ' num2str(model_nr) ': ' modelnames{model_nr} ', ID: ' num2str(ID) ', ROI: ' ROI.name{r} ' %%%%%%%%%% ' DESIGN.duration]); display(' ');
                        
                        DESIGN.output_dir = [DESIGN.Paths.MVPA '\2b_MVPAroi\' DESIGN.duration '\' modelnames{model_nr} '\' ROI.name{r} '\ID' num2str(ID) '\'];
                        if ~exist(DESIGN.output_dir)
                            mkdir(DESIGN.output_dir)
                            
                            
                            %% configure decoding
                            cfg = decoding_defaults();
                            regressor_names = design_from_spm(DESIGN.beta_dir);
                            
                            % find names of conditions by excluding motion regressors
                            labels = regressor_names(1,1:6);
                            labels = labels(find(ismember(labels, {'X', 'Y', 'Z', 'x', 'y', 'z'}) == 0));
                            
                            cfg = decoding_describe_data(cfg,labels,1:length(labels), regressor_names,DESIGN.beta_dir); % 1-1 are arbitrary label numbers for each condition
                            cfg.design = make_design_cv(cfg); %create design
                            %                 display_regressor_names(DESIGN.beta_dir)
                            
                            cfg.analysis ='roi'; %wholebrain, searchlight
                            
                            if r == 1 && (model_nr == 9 || model_nr == 10 || model_nr == 9)
                                cfg.plot_design = 1;
                            else
                                cfg.plot_design = 0;
                            end
                            
                            %% mask ROI
                            ROI.img_file = [ROI.dir 'ID' num2str(ID) '\' ROI.name{r} ROI.ImgType];
                            nrun = 1; % enter the number of runs here
                            jobfile = {'D:\Anne\2_analysis\jobs\maskROI_job.m'};
                            jobs = repmat(jobfile, 1, nrun);
                            inputs = cell(0, nrun);
                            for crun = 1:nrun
                            end
                            spm('defaults', 'FMRI');
                            spm_jobman('run', jobs, inputs{:});
                            
                            %% binarise ROI to exclude areas with high uncertainty
                            cd(DESIGN.output_dir);
                            fname='maskedROI';
                            ROI_hdr=spm_vol([fname '.img']);
                            ROI_vol=spm_read_vols(ROI_hdr);
                            ROI_hdr.pinfo = [1 1 1]';
                            
                            
                            ROI_hdr.fname=[DESIGN.output_dir 'maskedROI_bin.img'];
                            spm_write_vol(ROI_hdr,ROI_vol);
                            
                            
                            cfg.files.mask = {[DESIGN.output_dir  'maskedROI_bin.img']};%{[beta_dir 'mask.nii']};
                            
                            %                             design_check = decoding_load_data(cfg)
                            
                            %% or classification_kernel?
                            cfg.decoding.method = 'classification';
                            %'model_parameters'
                            if length(labels) == 2 %binary classification
                                cfg.results.output ={'accuracy_minus_chance'; 'AUC_minus_chance'; 'sensitivity_minus_chance'; 'specificity_minus_chance'; 'balanced_accuracy_minus_chance'; 'signed_decision_values'}; %confusion_matrix
                            else %multiclass classification
                                cfg.results.output ={'accuracy_minus_chance'; 'balanced_accuracy_minus_chance'};
                            end
                            
                            cfg.results.dir = DESIGN.output_dir;
                            cfg.results.overwrite = 1;
                            
                            %% Scale data
                            cfg.scale.method ='z'; %z-normalised, alternatively min0-max1
                            cfg.scale.estimation ='across'; %scaling estimated on training data and applied to test data
                            cfg.scale.cutoff =[-3 3];%outliers = +/- 3SD
                            cfg.scale.check_datatrans_ok = true;
                            
                            %% set C to 1 % parameters for libsvm (linear SV classification, cost = 1)
                            cfg.decoding.train.classification.model_parameters = '-s 0 -t 0 -c 1 -b 0 -q'; %= default
                            
                            cfg.verbose = 0; %(0: no output, 1: normal output [default], 2: high output).
                            cfg.plot_selected_voxels = 0;%10000; %plot searchlight of every 10000th step (default = 0 -> no plotting)
                            
                            %% DECODING
                            results = decoding(cfg);
                            
                            %% PERMUTATION test
                            cfg.results.setwise = 1;
                            cfg.design = make_design_permutation(cfg,length(labels)^(length(cfg.design.set))/2,1); %16 permutations
                            
                            cfg.stats.test = 'permutation'; %set test
                            cfg.stats.tail = 'right';
                            cfg.stats.output = {'accuracy_minus_chance'};
                            
                            [reference,cfg] = decoding(cfg);
                            
                            
                            %% plot results and save figure
                            %                             figure(99)
                            %                             set(gcf, 'Position', get(0, 'Screensize'));
                            %                             subplot(1,5,1);bar(results.accuracy_minus_chance.output); ylim([-50, 50]); title('ACC above chance');
                            %                             subplot(1,5,2);bar(results.balanced_accuracy_minus_chance.output); ylim([-50, 50]); title('ACC (balanced)');
                            %                             if length(labels) == 2 %binary classification
                            %                                 subplot(1,5,3);bar(results.AUC_minus_chance.output); ylim([-50, 50]); title('AUC above chance');
                            %                                 subplot(1,5,4);bar(results.sensitivity_minus_chance.output); ylim([-50, 50]); title('Sensitivity');
                            %                                 subplot(1,5,5);bar(results.specificity_minus_chance.output); ylim([-50, 50]); title('Specificity');
                            %                             end
                            %
                            %                             saveas(figure(99),[DESIGN.output_dir 'results.jpg'])
                            %                             KbWait
                            close all
                            %                             results.mask_index_each
                        end
                    else
                        display('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
                        display('%NOT ENOUGH RUNS!!');
                        display(['%%%%%% SKIPPING MODEL ' num2str(model_nr) ': ' modelnames{model_nr} ', ID: ' num2str(ID) '%%%%%%%%%% ' DESIGN.duration]); display(' ');
                        %                 WaitSecs(3);
                    end
                end
            end
        end
    end
end

% close all


