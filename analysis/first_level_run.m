%function first_level_run

clear
clc

pathdef

spm fmri

global PARAMS DESIGN MODEL ID model_nr

cd ..
cd '1_preprocessing'

display('%%%%%%%%%%%%%%%%%%%%%%% LOADING PREPROCESSING SET UP - SPM12 %%%%%%%%%%%%%%%%%%%%%');display(' ');
files = struct2cell(dir); files = any(ismember(files(1,:),'PreprocessingCfg.mat'));
if files == 0      % if not: run the next function to set up the general configurations
    Preprocessing_cfg;
elseif files == 1   % if: load configurations (steps, params, design)
    display('%%%% STEP 1: Define experimental design parameters.'); display(' ');
    display('Configuration file found. Step 2, Step 3 will be skipped and preprocessing will start shortly ...'); display(' ');
    load('PreprocessingCfg.mat');
end

DESIGN.Paths.analysis = ['C:\Users\action\Desktop\Anne\2_analysis\'];
DESIGN.Paths.analysisJobs = [DESIGN.Paths.analysis 'jobs'];

cd(DESIGN.Paths.analysis)

% which runs to analyse
DESIGN.DoRuns = {
    nan nan nan nan nan; %~1
    nan nan nan nan nan; %~2
    1 2 3 4 5; %ID 3
    1 2 3 4 5; %ID 4
    nan nan nan nan nan; %ID 5 --> excluded
    1 2 3 4 5; %ID 6
    1 2 3 4 5; %ID 7
    nan nan nan nan nan; %ID 8 --> excluded
    1 2 3 4 5; %ID 9
    1 2 3 4 5; %ID 10
    1 2 3 4 5; %ID 11
    1 2 3 4 5; %ID 12
    1 2 3 4 5; %ID 13
    1 2 3 4 5; %ID 14
    nan nan nan nan nan; %~15
    1 2 3 4 5; %ID 16
    nan nan nan nan nan; %~17
    nan nan nan nan nan; %~18
    1 2 3 4 5; %ID 19
    1 2 3 4 5; %ID 20
    1 2 3 4 5; %ID 21
    1 2 3 4 5; %ID 22
    nan nan nan nan nan; %ID 23 --> excluded
    1 2 3 4 5; %ID 24
    1 2 3 4 5; %ID 25
    1 2 3 4 5; %ID 26
    1 2 3 4 5; %ID 27
    1 2 3 4 nan; %ID 28
    1 2 3 4 5; %ID 29
    nan nan nan nan nan; %~30
    1 2 3 4 5; %ID 31
    1 2 3 4 5; %ID 32
    nan nan nan nan nan; %~33
    1 2 3 4 5; %ID 34
    1 2 3 4 5; %ID 35
    nan nan nan nan nan; %~36
    1 2 3 4 5; %ID 37
    };

DESIGN.DoUnivariate = 1; %run univariate analysis?
DESIGN.DoMultivariate = 0; %run multivariate analysis?

spm_jobman('initcfg');

for dur = 1%:2 %run everything for both event-related and with period durations
    if dur == 1
        DESIGN.duration = '1_event';
    elseif dur == 2
        DESIGN.duration = '2_period';
    end
    
    for i = 1:length(DESIGN.Participants)
        ID = DESIGN.Participants(i);
        if length(find(isnan(cell2mat(DESIGN.DoRuns(ID,:))))) < 5
            
            DESIGN.Paths.model = [DESIGN.Paths.analysis 'create design mat\models\'];
            DESIGN.Paths.preprocData = [DESIGN.Paths.preprocessing 'output\ID' num2str(ID) '\funcImages\'];%preprocessed data
            
            %Get Volume indices for each run
            DESIGN.runVols = {1:DESIGN.nVol{ID,1};...
                DESIGN.nVol{ID,1}+1:DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2};...
                DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2}+1:DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2}+DESIGN.nVol{ID,3};...
                DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2}+DESIGN.nVol{ID,3}+1:DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2}+DESIGN.nVol{ID,3}+DESIGN.nVol{ID,4};...
                DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2}+DESIGN.nVol{ID,3}+DESIGN.nVol{ID,4}+1:DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2}+DESIGN.nVol{ID,3}+DESIGN.nVol{ID,4}+DESIGN.nVol{ID,5}};
            
            %get motion regressors
            [r1,r2,r3,r4,r5,r6] = textread([DESIGN.Paths.preprocData 'rp_aRun1.txt'],'%f%f%f%f%f%f');
            reg = [r1,r2,r3,r4,r5,r6];
            %separate motion regressors for each run
            DESIGN.motReg{1} = reg(1:DESIGN.nVol{ID,1}, :);
            DESIGN.motReg{2} = reg(DESIGN.nVol{ID,1}+1:DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2}, :);
            DESIGN.motReg{3} = reg(DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2}+1:DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2}+DESIGN.nVol{ID,3}, :);
            DESIGN.motReg{4} = reg(DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2}+DESIGN.nVol{ID,3}+1:DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2}+DESIGN.nVol{ID,3}+DESIGN.nVol{ID,4}, :);
            DESIGN.motReg{5} = reg(DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2}+DESIGN.nVol{ID,3}+DESIGN.nVol{ID,4}+1:DESIGN.nVol{ID,1}+DESIGN.nVol{ID,2}+DESIGN.nVol{ID,3}+DESIGN.nVol{ID,4}+DESIGN.nVol{ID,5}, :);
            
            %specify models
            for model_nr = 32:33
                crit_nTrials = 1; %at least 3 trials per condition, otherwise, run is excluded
                for r=1:5
                    load([DESIGN.Paths.model 'ID' num2str(ID) '_Run' num2str(r) '.mat']);
                    switch model_nr
                        case 1
                            DESIGN.model_label{model_nr}='Fix1_ChoiceOn';
                            DESIGN.model{r} = MODEL.Fix1_ChoiceOn;
                            skip = 1;
                        case 2
                            DESIGN.model_label{model_nr}='Fix2_TargOn';
                            DESIGN.model{r} = MODEL.Fix2_TargOn;
                            skip = 1;
                        case 3
                            DESIGN.model_label{model_nr}='initDist_Close_Far';
                            DESIGN.model{r} = MODEL.targDist_init;
                            skip = 0;
                        case 4
                            DESIGN.model_label{model_nr}='finDist_Close_Far';
                            DESIGN.model{r} = MODEL.targDist_fin;
                            skip = 1;
                        case 5
                            DESIGN.model_label{model_nr}='initTargResp';
                            DESIGN.model{r} = MODEL.initTargResp;
                            skip = 1;
                        case 6
                            DESIGN.model_label{model_nr}='finTargResp';
                            DESIGN.model{r} = MODEL.finTargResp;
                            skip = 1;
                        case 7
                            DESIGN.model_label{model_nr}='initFace_House_choiceOn';
                            DESIGN.model{r} = MODEL.initFace_House_choiceOn;
                            skip = 0;
                        case 8
                            DESIGN.model_label{model_nr}='initFace_House_targOn';
                            DESIGN.model{r} = MODEL.initFace_House_targOn;
                            skip = 1;
                        case 9
                            DESIGN.model_label{model_nr}='finFace_House';
                            DESIGN.model{r} = MODEL.finFace_House;
                            skip = 1;
                        case 10
                            DESIGN.model_label{model_nr}='DiaParaNo_Reloc';
                            DESIGN.model{r} = MODEL.DiaParaNo_Reloc;
                            skip = 1;
                        case 11
                            DESIGN.model_label{model_nr}='noCoM_CoM_Reloc';
                            DESIGN.model{r} = MODEL.noCoM_CoM_Reloc;
                            skip = 1;
                        case 12
                            DESIGN.model_label{model_nr}='noCoM_CoM_Go';
                            DESIGN.model{r} = MODEL.noCoM_CoM_Go;
                            skip = 1;
                        case 13
                            DESIGN.model_label{model_nr}='noCoM_CoM_Resp';
                            DESIGN.model{r} = MODEL.noCoM_CoM_Resp;
                            skip = 1;
                        case 14
                            DESIGN.model_label{model_nr}='initTargRespBin';
                            DESIGN.model{r} = MODEL.initTargRespBin;
                            skip = 0;
                        case 15
                            DESIGN.model_label{model_nr}='finTargRespBin';
                            DESIGN.model{r} = MODEL.finTargRespBin;
                            skip = 0;
                        case 16
                            DESIGN.model_label{model_nr}='noCoM_CoM_Reloc_exclEarly';
                            DESIGN.model{r} = MODEL.noCoM_CoM_Reloc_exclEarly;
                            skip = 1;
                        case 17
                            DESIGN.model_label{model_nr}='finFace_House_CoM';
                            DESIGN.model{r} = MODEL.finFace_House_CoM;
                            skip = 1;
                        case 18
                            DESIGN.model_label{model_nr}='finFace_House_noCoM';
                            DESIGN.model{r} = MODEL.finFace_House_noCoM;
                            skip = 1;
                        case 19
                            DESIGN.model_label{model_nr}='finFace_House_CoM_excluded';
                            DESIGN.model{r} = MODEL.finFace_House_CoM_excluded;
                            skip = 1;
                        case 20
                            DESIGN.model_label{model_nr}='finFace_House_noCoM_excluded';
                            DESIGN.model{r} = MODEL.finFace_House_noCoM_excluded;
                            skip = 1;
                        case 21
                            DESIGN.model_label{model_nr}='noCoM_CoM_Reloc_exclEarlyError';
                            DESIGN.model{r} = MODEL.noCoM_CoM_Reloc_exclEarlyError;
                            skip = 0;
                        case 22
                            DESIGN.model_label{model_nr}='finFace_HouseDiag';
                            DESIGN.model{r} = MODEL.finFace_HouseDiag;
                            skip = 1;
                        case 23
                            DESIGN.model_label{model_nr}='initFace_House_choiceOn_delP';
                            DESIGN.model{r} = MODEL.initFace_House_choiceOn_delayP;
                            skip = 1;
                            
                        case 24
                            DESIGN.model_label{model_nr}='finFace_House_exclErrEarly';
                            DESIGN.model{r} = MODEL.finFace_House_exclErrEarly;
                            skip = 0;
                        case 25
                            DESIGN.model_label{model_nr}='targDist_fin_diag';
                            DESIGN.model{r} = MODEL.targDist_fin_diag;
                            skip = 1;
                        case 26
                            DESIGN.model_label{model_nr}='targDist_fin_exclErrEarly';
                            DESIGN.model{r} = MODEL.targDist_fin_exclErrEarly;
                            skip = 0;
                        case 27
                            DESIGN.model_label{model_nr}='noCoM_CoM_choiceOn_exclEarlyError';
                            DESIGN.model{r} = MODEL.noCoM_CoM_choiceOn_exclEarlyError;
                            skip = 1;
                        case 28
                            DESIGN.model_label{model_nr}='DecisionStage';
                            DESIGN.model{r} = MODEL.DecisionStage;
                            skip = 0;
                        case 29
                            DESIGN.model_label{model_nr}='DecisionStagexCoM';
                            DESIGN.model{r} = MODEL.DecisionStagexCoM;
                            skip = 0;
                        case 30
                            DESIGN.model_label{model_nr}='DecisionStage_tOnRel';
                            DESIGN.model{r} = MODEL.DecisionStage_tOnRel;
                            skip = 0;
                        case 31
                            DESIGN.model_label{model_nr}='DecisionStage_tOnRelxCoM';
                            DESIGN.model{r} = MODEL.DecisionStage_tOnRelxCoM;
                            skip = 0;
                        case 32
                            DESIGN.model_label{model_nr}='DecisionStage_chOntRel';
                            DESIGN.model{r} = MODEL.DecisionStage_chOntRel;
                            skip = 0;
                        case 33
                            DESIGN.model_label{model_nr}='DecisionStage_chOntRelxCoM';
                            DESIGN.model{r} = MODEL.DecisionStage_chOntRelxCoM;
                            skip = 0;
                    end
                    
                    %set DoRuns to 0 if exclude
                    %exclude certain runs for all models to begin with (e.g.,
                    %high error rate)
                    if isnan(DESIGN.DoRuns{ID,r})
                        DESIGN.ModelRuns{ID,r} = 0;
                    else
                        DESIGN.ModelRuns{ID,r} = r;
                        %if < 4 trials/cond -> exclude run, for that model only
                        for l=1:size(DESIGN.model{r}.onsets,2)
                            if size(DESIGN.model{r}.onsets{l},1) < crit_nTrials
                                DESIGN.ModelRuns{ID,r} = 0;
                            end
                        end
                    end
                    doRunsModel = {DESIGN.model_label{model_nr}; DESIGN.ModelRuns(ID,:)};
                end
                
                
                if length(find([DESIGN.ModelRuns{ID,:}]==0)) <= crit_nTrials %if at least 3 runs
                    
                    % parameters for first-level GLM
                    PARAMS.model{model_nr}.nLevels=size(DESIGN.model{1}.names,2); % refers to the numbers of onsets in all_onsets
                    PARAMS.model{model_nr}.param = zeros(1,PARAMS.model{model_nr}.nLevels);
                    PARAMS.model{model_nr}.nparam = sum(PARAMS.model{model_nr}.param);
                    PARAMS.model{model_nr}.cov = 6;
                    PARAMS.model{model_nr}.glm = 1; % HRF
                    PARAMS.model{model_nr}.param(1:(PARAMS.model{model_nr}.nLevels))=0;
                    PARAMS.model{model_nr}.nbetas_per_run = PARAMS.model{model_nr}.nLevels+...
                        PARAMS.model{model_nr}.nparam + PARAMS.model{model_nr}.cov; % 2 onset regressors, 0 paramentric regressors, 6 motion regressors = 8 regressors
                    
                    
                    DESIGN.Paths.resultsUniVar = [DESIGN.Paths.analysis '1_univariate\1_1st-Level\' DESIGN.duration '\' num2str(model_nr) '_' DESIGN.model_label{model_nr} '\ID' num2str(ID)];
                    
                    %% run univariate analysis
                    if DESIGN.DoUnivariate && ~exist(DESIGN.Paths.resultsUniVar) && skip == 0
                        display('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
                        display(['%%%%%% STARTING UNIVARIATE ANALYSIS, MODEL ' num2str(model_nr) ': ' DESIGN.model_label{model_nr} ', ID: ' num2str(ID) '%%%%%%%%%% ' DESIGN.duration]); display(' ');
                        mkdir(DESIGN.Paths.resultsUniVar)
                        
                        save([DESIGN.Paths.resultsUniVar '\ModelRuns'], 'doRunsModel');
                        
                        DESIGN.Paths.output = DESIGN.Paths.resultsUniVar;
                        MODEL.model.glm_images = 'swa';
                        [DESIGN.Vols1, DESIGN.Vols2, DESIGN.Vols3, DESIGN.Vols4, DESIGN.Vols5]= VolumesToInput;
                        
                        
                        % specify model
                        nrun = 1; % enter the number of runs here
                        jobfile = {[DESIGN.Paths.analysisJobs '\specify_job.m']};
                        jobs = repmat(jobfile, 1, nrun);
                        inputs = cell(0, nrun);
                        for crun = 1:nrun
                        end
                        spm('defaults', 'FMRI');
                        spm_jobman('run', jobs, inputs{:});
                        
                        % estimate model
                        nrun = 1; % enter the number of runs here
                        jobfile = {[DESIGN.Paths.analysisJobs '\estimate_job.m']};
                        jobs = repmat(jobfile, 1, nrun);
                        inputs = cell(0, nrun);
                        for crun = 1:nrun
                        end
                        spm('defaults', 'FMRI');
                        spm_jobman('run', jobs, inputs{:});
                        
                        % review model (saves PDF of Design matrix)
                        nrun = 1;
                        jobfile = {[DESIGN.Paths.analysisJobs '\review_job.m']};
                        cd(DESIGN.Paths.output)
                        jobs = repmat(jobfile, 1, nrun);
                        inputs = cell(0, nrun);
                        for crun = 1:nrun
                        end
                        spm('defaults', 'FMRI');
                        spm_jobman('run', jobs, inputs{:});
                        cd(DESIGN.Paths.analysis)
                        
                        % define contrasts
                        nrun = 1;
                        jobfile = {[DESIGN.Paths.analysisJobs '\contrasts_job.m']};
                        jobs = repmat(jobfile, 1, nrun);
                        inputs = cell(1, nrun);
                        for crun = 1:nrun
                        end
                        spm('defaults', 'FMRI');
                        spm_jobman('run', jobs, inputs{:});
                        
                        %write results
                        nrun = 1; % enter the number of runs here
                        jobfile = {[DESIGN.Paths.analysisJobs '\write_job.m']};
                        jobs = repmat(jobfile, 1, nrun);
                        inputs = cell(1, nrun);
                        for crun = 1:nrun
                        end
                        spm('defaults', 'FMRI');
                        spm_jobman('run', jobs, inputs{:});
                        
                        cd(DESIGN.Paths.analysis)
                    end
                    
                    %% run mutlivariate analysis
                    DESIGN.Paths.resultsMultiVar = [DESIGN.Paths.analysis '2_multivariate\1_1st-Level\' DESIGN.duration '\' num2str(model_nr) '_' DESIGN.model_label{model_nr} '\ID' num2str(ID)];
                    
                    if DESIGN.DoMultivariate && ~exist(DESIGN.Paths.resultsMultiVar) && skip == 0
                        display('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
                        display(['%%%%%% STARTING MULTIVARIATE ANALYSIS, MODEL ' num2str(model_nr) ': ' DESIGN.model_label{model_nr} ', ID: ' num2str(ID) '%%%%%%%%%% ' DESIGN.duration]); display(' ');
                        mkdir(DESIGN.Paths.resultsMultiVar)
                        save([DESIGN.Paths.resultsMultiVar '\ModelRuns'], 'doRunsModel');
                        
                        DESIGN.Paths.output = DESIGN.Paths.resultsMultiVar;
                        MODEL.model.glm_images = 'ra'; % 'ra' for mv, 'swa' for uv
                        [DESIGN.Vols1, DESIGN.Vols2, DESIGN.Vols3, DESIGN.Vols4, DESIGN.Vols5 ]= VolumesToInput;
                        
                        % specify model
                        nrun = 1; % enter the number of runs here
                        jobfile = {[DESIGN.Paths.analysisJobs '\specify_job.m']};
                        jobs = repmat(jobfile, 1, nrun);
                        inputs = cell(0, nrun);
                        for crun = 1:nrun
                        end
                        spm('defaults', 'FMRI');
                        spm_jobman('run', jobs, inputs{:});
                        
                        % estimate model
                        nrun = 1; % enter the number of runs here
                        jobfile = {[DESIGN.Paths.analysisJobs '\estimate_job.m']};
                        jobs = repmat(jobfile, 1, nrun);
                        inputs = cell(0, nrun);
                        for crun = 1:nrun
                        end
                        spm('defaults', 'FMRI');
                        spm_jobman('run', jobs, inputs{:});
                        
                        % review model (saves PDF of Design matrix)
                        nrun = 1;
                        jobfile = {[DESIGN.Paths.analysisJobs '\review_job.m']};
                        cd(DESIGN.Paths.output)
                        jobs = repmat(jobfile, 1, nrun);
                        inputs = cell(0, nrun);
                        for crun = 1:nrun
                        end
                        spm('defaults', 'FMRI');
                        spm_jobman('run', jobs, inputs{:});
                        cd(DESIGN.Paths.analysis)
                        
                        % define contrasts
                        nrun = 1;
                        jobfile = {[DESIGN.Paths.analysisJobs '\contrasts_job.m']};
                        jobs = repmat(jobfile, 1, nrun);
                        inputs = cell(1, nrun);
                        for crun = 1:nrun
                        end
                        spm('defaults', 'FMRI');
                        spm_jobman('run', jobs, inputs{:});
                        
                        %write results
                        nrun = 1; % enter the number of runs here
                        jobfile = {[DESIGN.Paths.analysisJobs '\write_job.m']};
                        jobs = repmat(jobfile, 1, nrun);
                        inputs = cell(1, nrun);
                        for crun = 1:nrun
                        end
                        spm('defaults', 'FMRI');
                        spm_jobman('run', jobs, inputs{:});
                        
                        cd(DESIGN.Paths.analysis)
                    end
                    
                else
                    display('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
                    display('%NOT ENOUGH RUNS!!');
                    display(['%%%%%% SKIPPING MODEL ' num2str(model_nr) ': ' DESIGN.model_label{model_nr} ', ID: ' num2str(ID) '%%%%%%%%%% ' DESIGN.duration]); display(' ');
                    %                 WaitSecs(3);
                end
            end
        end
    end
end

% end
