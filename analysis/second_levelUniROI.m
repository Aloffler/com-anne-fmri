clear
clc
close all

marsbar('on'); 

global ROI

ROI.analysis = '1_univariate\';
ROI.PathSecond = '2_2nd-level';

ROI.spm_dir = ['D:\Anne\2_analysis\' ROI.analysis ROI.PathSecond '\1_event\'];
ROI.model_name = '26_targDist_fin_exclErrEarly\finClose-Far - All Sessions';

% '3_initDist_Close_Far\initClose-Far - All Sessions'
% '4_finDist_Close_Far\finClose-Far - All Sessions'
% '7_initFace_House_choiceOn\iFace-iHouse_chOn - All Sessions'
% '9_finFace_House\fFace-fHouse - All Sessions'
% '11_noCoM_CoM_Reloc\CoM-noCoM_Reloc - All Sessions'
% '14_initTargRespBin\Left-Right - All Sessions'
% '15_finTargRespBin\Left-Right - All Sessions'
% '16_noCoM_CoM_Reloc_exclEarly\CoM-noCoM_Reloc_exclEarly - All Sessions'
% '17_finFace_House_CoM\fFace-fHouse_CoM - All Sessions'
% '18_finFace_House_noCoM\fFace-fHouse_noCoM - All Sessions'
% '21_noCoM_CoM_Reloc_exclEarlyError\CoM-noCoM_Reloc_exclEarlyError - All Sessions'
% '24_finFace_House_exclErrEarly\fFace-fHouse_diag - All Sessions'
% '26_targDist_fin_exclErrEarly\finClose-Far - All Sessions'
% '28_DecisionStage\targOn-choiceOn - All Sessions'


ROI.dir = 'D:\Anne\2_analysis\ROIs\';
ROI.name = {'MNI_Fusiform_RL_roi',...
        'MNI_Occipital_Inf_RL_roi',...
        'MNI_Precuneus_RL_roi',...
        'MNI_Angular_RL_roi',...
        'MNI_Frontal_Med_Orb_RL_roi',...
        'MNI_Frontal_Sup_Medial_RL_roi'};

ROI.output_dir = ([ROI.dir 'Results\' ROI.analysis '\' ROI.model_name '\']);

if ~exist(ROI.output_dir)
    mkdir(ROI.output_dir)
end

for i=1:length(ROI.name) 
    rois(i) = strcat([ROI.dir 'rois\'], strcat([ROI.name(i)], '.mat'));
end

%display ROIs and save fig
% mars_display_roi('display',rois); ROI.fig = ['ROI_' ROI.name{1}];
% % save roi plot
% nrun = 1; % enter the number of runs here
% jobfile = {'D:\Anne\2_analysis\ROIs\jobs\saveFig_job.m'};
% jobs = repmat(jobfile, 1, nrun);
% inputs = cell(0, nrun);
% for crun = 1:nrun
% end
% spm('defaults', 'FMRI');
% spm_jobman('run', jobs, inputs{:});

% Make marsbar design object
load([ROI.spm_dir ROI.model_name '\SPM.mat']);
D = mardo(SPM);

% Make marsbar ROI object
R = maroi(rois);

% save ROI as image
% for i=1:length(ROI.name)
%     save_as_image(R{i}, fullfile('D:\Anne\2_analysis\ROIs\ROIimg', sprintf([ROI.name{i} '.img'])));
% end

% Fetch data into marsbar data object
Y = get_marsy(R{:}, D, 'mean');

% Get contrasts from original design
xCon = get_contrasts(D);
xCon.c = 1;

% Estimate design on ROI data
E = estimate(D, Y);
% Put contrasts from original design back into design object
E = set_contrasts(E, xCon);
% get design betas
b = betas(E);
% get stats
marsS = compute_contrasts(E, 1:length(xCon))


% %% plot effect and CI & save fig
% r = 1;
% for r = 1:length(rois)
%     mars_spm_graph(E,r); ROI.fig = [ROI.name{r}];
%     KbWait;
%     %save roi plot
%     nrun = 1; % enter the number of runs here
%     jobfile = {'D:\Anne\2_analysis\ROIs\jobs\saveFig_job.m'};
%     jobs = repmat(jobfile, 1, nrun);
%     inputs = cell(0, nrun);
%     for crun = 1:nrun
%     end
%     spm('defaults', 'FMRI');
%     spm_jobman('run', jobs, inputs{:});
% end

display('')
display([ROI.name(find(marsS.P<.05)), ROI.name(find(marsS.P>.95))]')
