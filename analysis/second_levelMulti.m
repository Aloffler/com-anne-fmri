clear
clc

global DESIGN model_nr

cd 'D:\Anne\1_preprocessing'

display('%%%%%%%%%%%%%%%%%%%%%%% LOADING PREPROCESSING SET UP - SPM12 %%%%%%%%%%%%%%%%%%%%%');display(' ');
files = struct2cell(dir); files = any(ismember(files(1,:),'PreprocessingCfg.mat'));
if files == 0      % if not: run the next function to set up the general configurations
    Preprocessing_cfg;
elseif files == 1   % if: load configurations (steps, params, design)
    display('%%%% STEP 1: Define experimental design parameters.'); display(' '); WaitSecs(1.5);
    display('Configuration file found. Step 2, Step 3 will be skipped and preprocessing will start shortly ...'); display(' ');
    load('PreprocessingCfg.mat');
end

%% exclude participants
DESIGN.excludeIDs = {'ID5' 'ID8' 'ID23' 'ID24'};


DESIGN.Paths.analysis = ['D:\Anne\2_analysis\'];
DESIGN.Paths.analysisJobs = [DESIGN.Paths.analysis 'jobs_2nd'];

cd(DESIGN.Paths.analysis)

DESIGN.analysis = '2_multivariate';
DESIGN.cross = 0; %cross-classification?

for dur = 1:2
    
    if dur == 1
        DESIGN.dur = '1_event';
    elseif dur == 2
        DESIGN.dur = '2_period';
    end
    
    if DESIGN.cross
        DESIGN.Paths.outputFirst = [pwd '\' DESIGN.analysis '\2a_MVPAcross\' DESIGN.dur '\'];
        modelnames = {
            {'14_initTargRespBin', '15_finTargRespBin', '1_TargResp_cross'};
            {'7_initFace_House_choiceOn', '9_finFace_House', '2_fhChoice_cross'};
            {'3_initDist_Close_Far', '4_finDist_Close_Far', '3_TargDist_cross'}
            {'7_initFace_House_choiceOn', '22_finFace_HouseDiag', '2a_fhChoice_crossDiag'}};
        
    else
        DESIGN.Paths.outputFirst = [pwd '\' DESIGN.analysis '\2_MVPA\' DESIGN.dur '\'];
        models = dir(DESIGN.Paths.outputFirst);
        modelnames = {models.name};
        modelnames = modelnames(3:end);
        modelnames = sort_nat(modelnames);
    end
    
    for model_nr = 1:length(modelnames)
        if DESIGN.cross
            DESIGN.model_label{model_nr} = modelnames{model_nr}{3};
        else
            DESIGN.model_label{model_nr} = modelnames{model_nr};
        end
        DESIGN.contrastNr = 1;
        DESIGN.contrast = 1;
        if DESIGN.cross
            load([DESIGN.Paths.analysis DESIGN.analysis '\1_1st-Level\1_event\' modelnames{model_nr}{1} '\ID' num2str(DESIGN.Participants(1)) '\SPM.mat'])
        else
            load([DESIGN.Paths.analysis DESIGN.analysis '\1_1st-Level\1_event\' modelnames{model_nr} '\ID' num2str(DESIGN.Participants(1)) '\SPM.mat'])
        end
        
        DESIGN.contrastName = SPM.xCon(1,DESIGN.contrastNr).name;
        
        if DESIGN.cross
            DESIGN.Paths.outputSec = [DESIGN.Paths.analysis DESIGN.analysis '\3a_2nd-levelCross\' DESIGN.dur '\'  DESIGN.model_label{model_nr} '\' DESIGN.contrastName  '\'];
        else
            DESIGN.Paths.outputSec = [DESIGN.Paths.analysis DESIGN.analysis '\3_2nd-level\' DESIGN.dur '\'  DESIGN.model_label{model_nr} '\' DESIGN.contrastName  '\'];
        end
        
        DESIGN.imgNames = 'res_accuracy_minus_chance.nii'; %'res_specificity_minus_chance.nii'; %  %'res_sensitivity_minus_chance.nii'
        
        if ~exist(DESIGN.Paths.outputSec)
            mkdir(DESIGN.Paths.outputSec)
            
            %% normalise & smooth images
            display('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
            display('%%%%%%%%%%%%%%%%%%%%%%% NORMALISE & SMOOTH IMAGES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
            display('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'); display(' ');
            
            for i = 1:length(DESIGN.Participants)
                ID = DESIGN.Participants(i);
                if ID ~= 5 && ID ~= 8 && ID ~= 23 && ID ~= 24
                    
                    DESIGN.T1_img = [DESIGN.Paths.preprocessing 'output\ID' num2str(ID) '\T1\y_rT1.nii'];
                    DESIGN.firstLevel_img = [DESIGN.Paths.outputFirst DESIGN.model_label{model_nr} '\ID' num2str(ID) '\'];
                    
                    if ~exist([DESIGN.firstLevel_img 'sw' DESIGN.imgNames])
                        if exist([DESIGN.firstLevel_img DESIGN.imgNames])
                            nrun = 	1;
                            jobfile = {[DESIGN.Paths.analysisJobs '\Normalisation_job_MVPA.m']};
                            jobs = repmat(jobfile, 1, nrun);
                            inputs = cell(0, nrun);
                            for crun = 1:nrun
                            end
                            spm('defaults', 'FMRI');
                            spm_jobman('run', jobs, inputs{:});
                            
                            nrun = 1;
                            jobfile = {[DESIGN.Paths.analysisJobs '\Smoothing_job_MVPA.m']};
                            jobs = repmat(jobfile, 1, nrun);
                            inputs = cell(0, nrun);
                            for crun = 1:nrun
                            end
                            spm('defaults', 'FMRI');
                            spm_jobman('run', jobs, inputs{:});
                        end
                    end
                end
            end
            
            DESIGN.imgNames = ['sw' DESIGN.imgNames];
            
            %% specify
            % List of open inputs
            nrun = 1; % enter the number of runs here
            jobfile = {[DESIGN.Paths.analysisJobs '\specify_job.m']};
            jobs = repmat(jobfile, 1, nrun);
            inputs = cell(0, nrun);
            for crun = 1:nrun
            end
            spm('defaults', 'FMRI');
            spm_jobman('run', jobs, inputs{:});
            
            %% estimate
            % List of open inputs
            nrun = 1; % enter the number of runs here
            jobfile = {[DESIGN.Paths.analysisJobs '\estimate_job.m']};
            jobs = repmat(jobfile, 1, nrun);
            inputs = cell(0, nrun);
            for crun = 1:nrun
            end
            spm('defaults', 'FMRI');
            spm_jobman('run', jobs, inputs{:});
            
            %% define contrasts
            nrun = 1;
            jobfile = {[DESIGN.Paths.analysisJobs '\contrasts_job.m']};
            jobs = repmat(jobfile, 1, nrun);
            inputs = cell(1, nrun);
            for crun = 1:nrun
            end
            spm('defaults', 'FMRI');
            spm_jobman('run', jobs, inputs{:});
            
            %% results report
            % List of open inputs
            nrun = 1;
            jobfile = {[DESIGN.Paths.analysisJobs '\results_job.m']};
            jobs = repmat(jobfile, 1, nrun);
            inputs = cell(0, nrun);
            for crun = 1:nrun
            end
            spm('defaults', 'FMRI');
            spm_jobman('run', jobs, inputs{:});
            
            cd(DESIGN.Paths.analysis)
            
        end
    end
end
