%-----------------------------------------------------------------------
% Job saved on 22-Nov-2017 20:50:55 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6470)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
global DESIGN ID model_nr

matlabbatch{1}.spm.stats.results.spmmat = {[DESIGN.Paths.output '\SPM.mat']};
matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
matlabbatch{1}.spm.stats.results.conspec.contrasts = Inf;
matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'FWE';
matlabbatch{1}.spm.stats.results.conspec.thresh = 0.05;
matlabbatch{1}.spm.stats.results.conspec.extent = 3;
matlabbatch{1}.spm.stats.results.conspec.mask.image.name = '';%{[DESIGN.Paths.preprocData 'waRun1.nii,1']};
matlabbatch{1}.spm.stats.results.conspec.mask.image.mtype = 0;
matlabbatch{1}.spm.stats.results.units = 1;
matlabbatch{1}.spm.stats.results.print = 'pdf';
matlabbatch{1}.spm.stats.results.write.tspm.basename = [DESIGN.model_label{model_nr} '_ID' num2str(ID)];
