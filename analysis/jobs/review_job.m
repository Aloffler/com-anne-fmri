%-----------------------------------------------------------------------
% Job saved on 29-Nov-2017 11:38:12 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6906)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------

global DESIGN

matlabbatch{1}.spm.stats.review.spmmat = {[DESIGN.Paths.output '\SPM.mat']};
matlabbatch{1}.spm.stats.review.display.matrix = 1;
matlabbatch{1}.spm.stats.review.print = 'fig';


