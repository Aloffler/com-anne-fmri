%-----------------------------------------------------------------------
% Job saved on 14-Nov-2017 10:11:11 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6906)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------

global PARAMS DESIGN model_nr ID

matlabbatch{1}.spm.stats.fmri_spec.dir = {DESIGN.Paths.output}; %{'C:\Users\512ta-dlab\Desktop\Anne\1_univariate\2_1st Level\results\ID9'};
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2.2;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 16;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 8; 

% covariates (will create additional regressors per run, but not included in nonsets
[r1,r2,r3,r4,r5,r6] = textread([DESIGN.Paths.preprocData 'rp_aRun1.txt'],'%f%f%f%f%f%f');

sess = 0;
%% INPUT FOR EACH RUN
%% RUN 1
run = 1;
if DESIGN.ModelRuns{ID,run} > 0
    sess = sess+1; %SPM session (independent of run because some runs excluded)
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).scans = DESIGN.Vols1;
    for i = 1:PARAMS.model{model_nr}.nLevels %specify each level of factor
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).name = DESIGN.model{run}.names{i};
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).onset = DESIGN.model{run}.onsets{:,i};
        if DESIGN.duration(1) == '1' %event
            matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).duration = DESIGN.model{run}.durations_event{:,i};
        elseif DESIGN.duration(1) == '2' %period
            matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).duration = DESIGN.model{run}.durations_period{:,i};
        end
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).tmod = 0;
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).pmod = struct('name', {}, 'param', {}, 'poly', {});
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).orth = 1;
    end
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).multi = {''};
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(1).name = 'X';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(1).val = r1(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(2).name = 'Y';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(2).val = r2(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(3).name = 'Z';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(3).val = r3(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(4).name = 'x';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(4).val = r4(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(5).name = 'y';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(5).val = r5(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(6).name = 'z';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(6).val = r6(DESIGN.runVols{run});
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).multi_reg = {''};
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).hpf = 128;
end

%% RUN 2
run = 2;
if DESIGN.ModelRuns{ID,run} > 0
    sess = sess+1; %SPM session (independent of run because some runs excluded)
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).scans = DESIGN.Vols2;
    for i = 1:PARAMS.model{model_nr}.nLevels %specify each level of factor
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).name = DESIGN.model{run}.names{i};
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).onset = DESIGN.model{run}.onsets{:,i};
        if DESIGN.duration(1) == '1' %event
            matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).duration = DESIGN.model{run}.durations_event{:,i};
        elseif DESIGN.duration(1) == '2' %period
            matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).duration = DESIGN.model{run}.durations_period{:,i};
        end
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).tmod = 0;
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).pmod = struct('name', {}, 'param', {}, 'poly', {});
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).orth = 1;
    end
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).multi = {''};
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(1).name = 'X';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(1).val = r1(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(2).name = 'Y';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(2).val = r2(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(3).name = 'Z';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(3).val = r3(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(4).name = 'x';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(4).val = r4(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(5).name = 'y';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(5).val = r5(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(6).name = 'z';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(6).val = r6(DESIGN.runVols{run});
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).multi_reg = {''};
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).hpf = 128;
end

%% RUN 3
run = 3;
if DESIGN.ModelRuns{ID,run} > 0
    sess = sess+1; %SPM session (independent of run because some runs excluded)
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).scans = DESIGN.Vols3;
    for i = 1:PARAMS.model{model_nr}.nLevels %specify each level of factor
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).name = DESIGN.model{run}.names{i};
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).onset = DESIGN.model{run}.onsets{:,i};
        if DESIGN.duration(1) == '1' %event
            matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).duration = DESIGN.model{run}.durations_event{:,i};
        elseif DESIGN.duration(1) == '2' %period
            matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).duration = DESIGN.model{run}.durations_period{:,i};
        end
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).tmod = 0;
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).pmod = struct('name', {}, 'param', {}, 'poly', {});
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).orth = 1;
    end
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).multi = {''};
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(1).name = 'X';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(1).val = r1(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(2).name = 'Y';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(2).val = r2(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(3).name = 'Z';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(3).val = r3(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(4).name = 'x';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(4).val = r4(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(5).name = 'y';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(5).val = r5(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(6).name = 'z';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(6).val = r6(DESIGN.runVols{run});
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).multi_reg = {''};
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).hpf = 128;
end

%% RUN 4
run = 4;
if DESIGN.ModelRuns{ID,run} > 0
    sess = sess+1; %SPM session (independent of run because some runs excluded)
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).scans = DESIGN.Vols4;
    for i = 1:PARAMS.model{model_nr}.nLevels %specify each level of factor
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).name = DESIGN.model{run}.names{i};
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).onset = DESIGN.model{run}.onsets{:,i};
        if DESIGN.duration(1) == '1' %event
            matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).duration = DESIGN.model{run}.durations_event{:,i};
        elseif DESIGN.duration(1) == '2' %period
            matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).duration = DESIGN.model{run}.durations_period{:,i};
        end
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).tmod = 0;
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).pmod = struct('name', {}, 'param', {}, 'poly', {});
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).orth = 1;
    end
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).multi = {''};
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(1).name = 'X';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(1).val = r1(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(2).name = 'Y';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(2).val = r2(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(3).name = 'Z';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(3).val = r3(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(4).name = 'x';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(4).val = r4(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(5).name = 'y';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(5).val = r5(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(6).name = 'z';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(6).val = r6(DESIGN.runVols{run});
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).multi_reg = {''};
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).hpf = 128;
end

%% RUN 5
run = 5;
if DESIGN.ModelRuns{ID,run} > 0
    sess = sess+1; %SPM session (independent of run because some runs excluded)
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).scans = DESIGN.Vols5;
    for i = 1:PARAMS.model{model_nr}.nLevels %specify each level of factor
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).name = DESIGN.model{run}.names{i};
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).onset = DESIGN.model{run}.onsets{:,i};
        if DESIGN.duration(1) == '1' %event
            matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).duration = DESIGN.model{run}.durations_event{:,i};
        elseif DESIGN.duration(1) == '2' %period
            matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).duration = DESIGN.model{run}.durations_period{:,i};
        end
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).tmod = 0;
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).pmod = struct('name', {}, 'param', {}, 'poly', {});
        matlabbatch{1}.spm.stats.fmri_spec.sess(sess).cond(i).orth = 1;
    end
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).multi = {''};
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(1).name = 'X';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(1).val = r1(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(2).name = 'Y';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(2).val = r2(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(3).name = 'Z';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(3).val = r3(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(4).name = 'x';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(4).val = r4(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(5).name = 'y';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(5).val = r5(DESIGN.runVols{run})';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(6).name = 'z';
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).regress(6).val = r6(DESIGN.runVols{run});
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).multi_reg = {''};
    matlabbatch{1}.spm.stats.fmri_spec.sess(sess).hpf = 128;
end

%% MODEL PARAMETERS
matlabbatch{1}.spm.stats.fmri_spec.fact.name = DESIGN.model_label{model_nr};
matlabbatch{1}.spm.stats.fmri_spec.fact.levels = PARAMS.model{model_nr}.nLevels;
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0]; %PARAMS.model{model_nr}.param;
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
matlabbatch{1}.spm.stats.fmri_spec.mask = {''}; %% MASK???
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';
