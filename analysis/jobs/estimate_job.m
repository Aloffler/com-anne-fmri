%-----------------------------------------------------------------------
% Job saved on 14-Nov-2017 12:07:47 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6906)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
global DESIGN

% global normalization: 'Scaling'|'None'
SPM.xGX.iGXcalc = 'None';
% low frequency confound: high-pass cutoff (secs) [Inf = no filtering]
SPM.xX.K.HParam = 128;
% intrinsic autocorrelations: OPTIONS: 'none'|'AR(1) + w'
SPM.xVi.form = 'none';
    
matlabbatch{1}.spm.stats.fmri_est.spmmat = {[DESIGN.Paths.output '\SPM.mat']}; %{'C:\Users\512ta-dlab\Desktop\Anne\1_univariate\2_1st Level\results\ID9\SPM.mat'};
matlabbatch{1}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{1}.spm.stats.fmri_est.method.Classical = 1;
