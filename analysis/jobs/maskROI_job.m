%-----------------------------------------------------------------------
% Job saved on 25-Jan-2018 16:45:12 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6906)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
global DESIGN ROI

matlabbatch{1}.spm.util.imcalc.input = {[DESIGN.beta_dir 'mask.nii']; [ROI.img_file ',1']};% ['D:\Anne\1_preprocessing\output\ID' num2str(ID) '\T1\brainMask.nii,1']};
%{[ROI.img_file ',1']; ['D:\Anne\1_preprocessing\output\ID' num2str(ID) '\T1\brainMask.nii,1']};
                                    
matlabbatch{1}.spm.util.imcalc.output = 'maskedROI.img';
matlabbatch{1}.spm.util.imcalc.outdir = {DESIGN.output_dir};
matlabbatch{1}.spm.util.imcalc.expression = '(i2.*i1)'; 
matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
matlabbatch{1}.spm.util.imcalc.options.mask = 0;
matlabbatch{1}.spm.util.imcalc.options.interp = 1;
matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
