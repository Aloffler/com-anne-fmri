%-----------------------------------------------------------------------
% Job saved on 29-Nov-2017 11:45:16 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6906)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------

global DESIGN model_nr

matlabbatch{1}.spm.stats.con.spmmat = {[DESIGN.Paths.output '\SPM.mat']};

switch model_nr
    case 1 %'Fix1_ChoiceOn'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'choiceOn-Fix1';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
    case 2 %'Fix2_TargOn'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'TargOn-Fix2';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
    case 3 %'initDist_Close_Far'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'initClose-Far';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'initFar-Close';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 4 %'finDist_Close_Far'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'finClose-Far';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'finFar-Close';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 5 %'initTargResp';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'iTarg1-others';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [3 -1 -1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'iTarg2-others';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 3 -1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'iTarg3-others';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [-1 -1 3 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.name = 'iTarg4-others';
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.weights = [-1 -1 -1 3 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{5}.tcon.name = 'iRightTarg-iLeftTarg';
        matlabbatch{1}.spm.stats.con.consess{5}.tcon.weights = [-1 1 1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{5}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{6}.tcon.name = 'iLeftTarg-iRightTarg';
        matlabbatch{1}.spm.stats.con.consess{6}.tcon.weights = [1 -1 -1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{6}.tcon.sessrep = 'replsc';
    case 6 %'finTargResp'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'fTarg1-others';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [3 -1 -1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'fTarg2-others';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 3 -1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'fTarg3-others';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [-1 -1 3 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.name = 'fTarg4-others';
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.weights = [-1 -1 -1 3 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{5}.tcon.name = 'fRightTarg-fLeftTarg';
        matlabbatch{1}.spm.stats.con.consess{5}.tcon.weights = [-1 1 1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{5}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{6}.tcon.name = 'fLeftTarg-fRightTarg';
        matlabbatch{1}.spm.stats.con.consess{6}.tcon.weights = [1 -1 -1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{6}.tcon.sessrep = 'replsc';
    case 7 %'initFace_House_choiceOn'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'iFace-iHouse_chOn';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'iHouse-iFace_chOn';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 8 %'initFace_House_targOn'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'iFace-iHouse_tOn';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'iHouse-iFace_tOn';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 9 %'finFace_House'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'fFace-fHouse';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'fHouse-fFace';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 10 %'DiaParaNo_Reloc'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Diag-noReloc';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 0 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Diag-Para';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 -1 0 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'Para-noReloc';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [0 1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.name = 'Para-Diag';
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.weights = [-1 1 0 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{5}.tcon.name = 'Reloc-noReloc';
        matlabbatch{1}.spm.stats.con.consess{5}.tcon.weights = [1 1 -2 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{5}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{6}.tcon.name = 'noReloc-Reloc';
        matlabbatch{1}.spm.stats.con.consess{6}.tcon.weights = [-1 -1 2 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{6}.tcon.sessrep = 'replsc';
    case 11 %'noCoM_CoM_Reloc'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'CoM-noCoM_Reloc';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'noCoM-CoM_Reloc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 12 %'noCoM_CoM_Go'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'CoM-noCoM_Go';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'noCoM-CoM_Go';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 13 %'noCoM_CoM_Resp'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'CoM-noCoM_Resp';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'noCoM-CoM_Resp';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 14 %'initTargRespBinInit'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Right-Left';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Left-Right';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 15 %'initTargRespBinFin'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Right-Left';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Left-Right';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 16 %'noCoM_CoM_Reloc' - excluding early CoM
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'CoM-noCoM_Reloc_exclEarly';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'noCoM-CoM_Reloc_exclEarly';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 17 %'finFace_House_CoM'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'fFace-fHouse_CoM';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'fHouse-fFace_CoM';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 18 %'finFace_House_noCoM'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'fFace-fHouse_noCoM';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'fHouse-fFace_noCoM';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 19 %'finFace_House_CoM_excluded'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'fFace-fHouse_CoM_excluded';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'fHouse-fFace_CoM_excluded';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 20 %'finFace_House_noCoM_excluded'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'fFace-fHouse_noCoM_excluded';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'fHouse-fFace_noCoM_excluded';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 21 %'noCoM_CoM_Reloc' - excluding early CoM & errors
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'CoM-noCoM_Reloc_exclEarlyError';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'noCoM-CoM_Reloc_exclEarlyError';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 22 %'finFace_HouseDiag'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'fFace-fHouse_diag';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'fHouse-fFace_diag';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 23 %'initFace_House_choiceOn - including delay'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'iFace-iHouse_chOn_delP';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'iHouse-iFace_chOn_delP';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
        
    case 24 %'finFace_House_exclErrEarly'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'fFace-fHouse';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'fHouse-fFace';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
        
    case 25 %'targDist_fin_diag'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'fFace-fHouse';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'fHouse-fFace';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
        
    case 26 %'finDist_Close_Far'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'finClose-Far';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'finFar-Close';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 27 %'noCoM_CoM_choiceOn' - locked to trial start, excluding early CoM & errors
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'CoM-noCoM_choiceOn_exclEarlyError';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'noCoM-CoM_choiceOn_exclEarlyError';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 28 %'DecisionStage'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'targOn-choiceOn';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'targRel-targOn';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [0 -1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'targRel-choiceOn';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [-1 0 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'replsc';
    case 29 %'DecisionStagexCoM'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'targOn-choiceOn_CoM';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'targRel-targOn_CoM';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [0 -1 1 0 0 0 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'targRel-choiceOn_CoM';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [-1 0 1 0 0 0 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.name = 'targOn-choiceOn_noCoM';
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.weights = [0 0 0 -1 1 0 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{5}.tcon.name = 'targRel-targOn_noCoM';
        matlabbatch{1}.spm.stats.con.consess{5}.tcon.weights = [0 0 0 0 -1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{5}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{6}.tcon.name = 'targRel-choiceOn_noCoM';
        matlabbatch{1}.spm.stats.con.consess{6}.tcon.weights = [0 0 0 -1 0 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{6}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{7}.tcon.name = 'CoM_noCoM';
        matlabbatch{1}.spm.stats.con.consess{7}.tcon.weights = [1 1 1 -1 -1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{7}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{8}.tcon.name = 'CoM_noCoM_Reloc';
        matlabbatch{1}.spm.stats.con.consess{8}.tcon.weights = [0 0 1 0 0 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{8}.tcon.sessrep = 'replsc';
    case 30 %'DecisionStage_tOnRel'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'targOn-targRel';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1  0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'targRel-targOn';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 31 %'DecisionStage_tOnRelxCoM'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'targRel-targOn_CoM';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'targRel-targOn_noCoM';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [0 0 -1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'targOn-targRel_CoM';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [1 -1 0 0 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.name = 'targOn-targRel_noCoM';
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.weights = [0 0 1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.sessrep = 'replsc';
    case 32 %'DecisionStage_chOntRel'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'chOn-targRel';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1  0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'targRel-chOn';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
    case 33 %'DecisionStage_chOntRelxCoM'
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'targRel-chOn_CoM';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [-1 1 0 0 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'targRel-chOn_noCoM';
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [0 0 -1 1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'chOn-targRel_CoM';
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [1 -1 0 0 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'replsc';
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.name = 'chOn-targRel_noCoM';
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.weights = [0 0 1 -1 0 0 0 0 0 0];
        matlabbatch{1}.spm.stats.con.consess{4}.tcon.sessrep = 'replsc';
end

matlabbatch{1}.spm.stats.con.delete = 1;
