function decodeSearchlight

close all
clc

global DESIGN
cd 'D:\Anne\1_preprocessing'

display('%%%%%%%%%%%%%%%%%%%%%%% LOADING PREPROCESSING SET UP - SPM12 %%%%%%%%%%%%%%%%%%%%%');display(' ');
files = struct2cell(dir); files = any(ismember(files(1,:),'PreprocessingCfg.mat'));
if files == 0      % if not: run the next function to set up the general configurations
    Preprocessing_cfg;
elseif files == 1   % if: load configurations (steps, params, design)
    display('%%%% STEP 1: Define experimental design parameters.'); display(' '); WaitSecs(1.5);
    display('Configuration file found. Step 2, Step 3 will be skipped and preprocessing will start shortly ...'); display(' ');
    load('PreprocessingCfg.mat');
end

DESIGN.Paths.analysis = ['D:\Anne\2_analysis\'];
DESIGN.Paths.analysisJobs = [DESIGN.Paths.analysis 'jobs'];

DESIGN.Paths.MVPA = 'D:\Anne\2_analysis\2_multivariate';
DESIGN.Paths.firstLevel = 'D:\Anne\2_analysis\2_multivariate\1_1st-Level\';

cd(DESIGN.Paths.MVPA)

t0 = GetSecs;

for dur = 1%:2
    
    switch dur
        case 1
            DESIGN.duration = '1_event';
        case 2
            DESIGN.duration = '2_period';
    end
    
    models = dir([DESIGN.Paths.firstLevel DESIGN.duration]);
    modelnames = {models.name}';
    modelnames = modelnames(3:end);
    modelnames = sort_nat(modelnames);
    
    for i = 1:length(DESIGN.Participants)
        ID = DESIGN.Participants(i);
        
        for model_nr = 15%3:5%length(modelnames)
            
            beta_dir = [DESIGN.Paths.firstLevel DESIGN.duration '\' modelnames{model_nr} '\ID' num2str(ID) '\'];
            
            if exist(beta_dir)
                display('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
                display(['%%%%%% START DECODING, MODEL ' num2str(model_nr) ': ' modelnames{model_nr} ', ID: ' num2str(ID) '%%%%%%%%%% ' DESIGN.duration]); display(' ');
                
                output_dir = [DESIGN.Paths.MVPA '\2_MVPA\' DESIGN.duration '\' modelnames{model_nr} '\ID' num2str(ID) '\'];
                if ~exist(output_dir)
                    mkdir(output_dir)
                    
                    
                    %% configure decoding
                    cfg = decoding_defaults();
                    
                    %for correlation analysis
%                     cfg.decoding.software = 'correlation_classifier';
%                     cfg.decoding.method = 'classification';

                    regressor_names = design_from_spm(beta_dir);
                    
                    % find names of conditions by excluding motion regressors
                    labels = regressor_names(1,1:6);
                    labels = labels(find(ismember(labels, {'X', 'Y', 'Z', 'x', 'y', 'z'}) == 0));
                    
                    cfg = decoding_describe_data(cfg,labels,1:length(labels), regressor_names,beta_dir); % 1-1 are arbitrary label numbers for each condition
                    cfg.design = make_design_cv(cfg); %create design
                    %                 display_regressor_names(beta_dir)
                    
                    cfg.analysis ='searchlight'; %wholebrain, ROI
                    cfg.searchlight.unit = 'voxels'; % comment or set to 'mm' if you want normal voxels
                    cfg.searchlight.radius = 4;
                    cfg.searchlight.spherical = 1;%0=default, set to 1???
                    %             cfg.searchlight.subset = [repmat(kron([1:50]',ones(9,1)),1,1) repmat(kron([1:50]',ones(3,1)),3,1) repmat(kron([1:50]',ones(1,1)),9,1)]; % x/y/z coordinates of middle voxel (or provide single index)
                    
                    cfg.files.mask = {[beta_dir 'mask.nii']};
                    
                    %% or classification_kernel?
                    cfg.decoding.method = 'classification';
                    %'model_parameters'
                    if length(labels) == 2 %binary classification
                        cfg.results.output ={'accuracy_minus_chance'; 'AUC_minus_chance'; 'sensitivity_minus_chance'; 'specificity_minus_chance'; 'balanced_accuracy_minus_chance'; 'signed_decision_values'}; %confusion_matrix
                    else %multiclass classification
                        cfg.results.output ={'accuracy_minus_chance'; 'balanced_accuracy_minus_chance'}; 
                    end
                    
                    cfg.results.dir = output_dir;
                    cfg.results.overwrite = 1;
                    
                    %% Scale data
                    cfg.scale.method ='z'; %z-normalised, alternatively min0-max1
                    cfg.scale.estimation ='across'; %scaling estimated on training data and applied to test data
                    cfg.scale.cutoff =[-3 3];%outliers = +/- 3SD
                    cfg.scale.check_datatrans_ok = true;
                    
                    %% set C to 1 % parameters for libsvm (linear SV classification, cost = 1)
                    cfg.decoding.train.classification.model_parameters = '-s 0 -t 0 -c 1 -b 0 -q'; %= default

                    cfg.verbose = 1; %(0: no output, 1: normal output [default], 2: high output).
                    cfg.plot_selected_voxels = 0;%10000; %plot searchlight of every 10000th step (default = 0 -> no plotting)
                    
                    %% DECODING
                    results = decoding(cfg);
                    
                    %                 %% PERMUTATION test
                    %                 cfg.results.setwise = 1;
                    %                 cfg.design = make_design_permutation(cfg,16,1); %16 permutations
                    %                 [reference,cfg] = decoding(cfg);
                    %                 cfg.stats.test = 'permutation'; %set test
                    %                 cfg.stats.tail = 'right';
                    %                 cfg.stats.output = {'accuracy_minus_chance'};
                    %                 p = decoding_statistics(cfg,results,reference);
                    %                 save([output_dir 'permutation', 'p']);
%                     
%                     %% plot results and save figure
%                     figure(99)
%                     set(gcf, 'Position', get(0, 'Screensize'));
%                     subplot(1,5,1);hist(results.accuracy_minus_chance.output); title('ACC above chance');
%                     subplot(1,5,2);hist(results.balanced_accuracy_minus_chance.output); title('ACC (balanced)');
%                     if length(labels) == 2 %binary classification
%                         subplot(1,5,3);hist(results.AUC_minus_chance.output); title('AUC above chance');
%                         subplot(1,5,4);hist(results.sensitivity_minus_chance.output); title('Sensitivity');
%                         subplot(1,5,5);hist(results.specificity_minus_chance.output); title('Specificity');
%                     end
%                     
%                     saveas(figure(99),[output_dir 'results.jpg'])
                    close all
                end
            else
                display('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
                display('%NOT ENOUGH RUNS!!');
                display(['%%%%%% SKIPPING MODEL ' num2str(model_nr) ': ' modelnames{model_nr} ', ID: ' num2str(ID) '%%%%%%%%%% ' DESIGN.duration]); display(' ');
%                 WaitSecs(3);
            end
        end
    end
end

display(['*** DURATION: ' num2str((GetSecs-t0)/60) ' minutes ***']);

close all


