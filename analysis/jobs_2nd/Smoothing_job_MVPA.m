%-----------------------------------------------------------------------
% Job saved on 06-Jul-2016 15:57:22 by cfg_util (rev $Rev: 6134 $)
% spm SPM - SPM12 (6225)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
%%
global PARAMS DESIGN

matlabbatch{1}.spm.spatial.smooth.data = {[DESIGN.firstLevel_img 'w' DESIGN.imgNames]};
%%
matlabbatch{1}.spm.spatial.smooth.fwhm = [PARAMS.smooth.fwhm];
matlabbatch{1}.spm.spatial.smooth.dtype = 0;
matlabbatch{1}.spm.spatial.smooth.im = 0;
matlabbatch{1}.spm.spatial.smooth.prefix = 's';
