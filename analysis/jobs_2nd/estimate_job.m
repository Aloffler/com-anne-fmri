%-----------------------------------------------------------------------
% Job saved on 07-Dec-2017 11:19:40 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6906)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
global DESIGN
matlabbatch{1}.spm.stats.fmri_est.spmmat = {[DESIGN.Paths.outputSec 'SPM.mat']};
matlabbatch{1}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{1}.spm.stats.fmri_est.method.Classical = 1;
