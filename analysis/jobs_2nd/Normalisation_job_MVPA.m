%-----------------------------------------------------------------------
% Job saved on 06-Jul-2016 15:51:26 by cfg_util (rev $Rev: 6134 $)
% spm SPM - SPM12 (6225)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
global DESIGN

matlabbatch{1}.spm.spatial.normalise.write.subj.def = {DESIGN.T1_img};
%%


matlabbatch{1}.spm.spatial.normalise.write.subj.resample = {[DESIGN.firstLevel_img DESIGN.imgNames]};
%%
matlabbatch{1}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70
                                                          78 76 85];
matlabbatch{1}.spm.spatial.normalise.write.woptions.vox = [3 3 3];
matlabbatch{1}.spm.spatial.normalise.write.woptions.interp = 4;
