%-----------------------------------------------------------------------
% Job saved on 14-Dec-2017 18:04:59 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6906)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
global DESIGN
matlabbatch{1}.spm.stats.results.spmmat = {[DESIGN.Paths.outputSec 'SPM.mat']};
matlabbatch{1}.spm.stats.results.conspec.titlestr = DESIGN.contrastName;
matlabbatch{1}.spm.stats.results.conspec.contrasts = 1;
matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'FWE';
matlabbatch{1}.spm.stats.results.conspec.thresh = 0.05;
matlabbatch{1}.spm.stats.results.conspec.extent = 3;
matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
matlabbatch{1}.spm.stats.results.units = 1;
matlabbatch{1}.spm.stats.results.export{1}.pdf = true;
matlabbatch{1}.spm.stats.results.export{2}.ps = true;
matlabbatch{1}.spm.stats.results.export{3}.tspm.basename = DESIGN.contrastName;
