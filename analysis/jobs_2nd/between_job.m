%-----------------------------------------------------------------------
% Job saved on 08-Jan-2018 12:29:06 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6906)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------

global DESIGN

matlabbatch{1}.spm.stats.factorial_design.dir = {DESIGN.Paths.outputSec};


for i = 1:length(DESIGN.high_dPrime)  
    matlabbatch{1}.spm.stats.factorial_design.des.t2.scans1(i,1) = {['D:\Anne\2_analysis\2_multivariate\2_MVPA\2_period\11_noCoM_CoM_Reloc\' DESIGN.high_dPrime{i} '\' DESIGN.imgNames ',1']};
end

for i = 1:length(DESIGN.low_dPrime)
    matlabbatch{1}.spm.stats.factorial_design.des.t2.scans2(i,1) = {['D:\Anne\2_analysis\2_multivariate\2_MVPA\2_period\11_noCoM_CoM_Reloc\' DESIGN.low_dPrime{i} '\' DESIGN.imgNames ',1']};
end
                                                       
matlabbatch{1}.spm.stats.factorial_design.des.t2.dept = 0;
matlabbatch{1}.spm.stats.factorial_design.des.t2.variance = 1;
matlabbatch{1}.spm.stats.factorial_design.des.t2.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.t2.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 0;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {''};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
