%-----------------------------------------------------------------------
% Job saved on 14-Dec-2017 18:47:20 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6906)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
global DESIGN
matlabbatch{1}.spm.stats.con.spmmat = {[DESIGN.Paths.outputSec 'SPM.mat']};
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = DESIGN.contrastName;
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 0;
