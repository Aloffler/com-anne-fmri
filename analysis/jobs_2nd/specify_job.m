%-----------------------------------------------------------------------
% Job saved on 05-Dec-2017 09:21:12 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6906)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------

global DESIGN model_nr

matlabbatch{1}.spm.stats.factorial_design.dir = {DESIGN.Paths.outputSec};
%%
files = dir([DESIGN.Paths.outputFirst DESIGN.model_label{model_nr}]);
filenames = {files.name}';
filenames = filenames(3:end);

%exclude participants
filenames(ismember(filenames, DESIGN.excludeIDs)) = []; 

for k = 1:length(filenames)
    filenames{k} = strcat([DESIGN.Paths.outputFirst], strcat(DESIGN.model_label{model_nr}, ['\' filenames{k} '\' DESIGN.imgNames]));
end

matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = filenames;
%%
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {''};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
