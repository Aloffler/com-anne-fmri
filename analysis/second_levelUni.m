clear
clc

global DESIGN model_nr

cd 'D:\Anne\1_preprocessing'

display('%%%%%%%%%%%%%%%%%%%%%%% LOADING PREPROCESSING SET UP - SPM12 %%%%%%%%%%%%%%%%%%%%%');display(' ');
files = struct2cell(dir); files = any(ismember(files(1,:),'PreprocessingCfg.mat'));
if files == 0      % if not: run the next function to set up the general configurations
    Preprocessing_cfg;
elseif files == 1   % if: load configurations (steps, params, design)
    display('%%%% STEP 1: Define experimental design parameters.'); display(' '); WaitSecs(1.5);
    display('Configuration file found. Step 2, Step 3 will be skipped and preprocessing will start shortly ...'); display(' ');
    load('PreprocessingCfg.mat');
end

%% exclude participants
DESIGN.excludeIDs = {'ID5' 'ID8' 'ID23' 'ID24'};

DESIGN.Paths.analysis = ['D:\Anne\2_analysis\'];
DESIGN.Paths.analysisJobs = [DESIGN.Paths.analysis 'jobs_2nd'];

cd(DESIGN.Paths.analysis)

DESIGN.analysis = '1_univariate';



for dur = 1:2
    
    if dur == 1
        DESIGN.dur = '1_event';
    elseif dur == 2
        DESIGN.dur = '2_period';
    end
    
    DESIGN.Paths.outputFirst = [pwd '\' DESIGN.analysis '\1_1st-level\' DESIGN.dur '\'];
    
    models = dir(DESIGN.Paths.outputFirst);
    modelsnames = {models.name};
    modelsnames = modelsnames(3:end);
    modelsnames = sort_nat(modelsnames);
    
    for model_nr = 1:size(modelsnames,2)
        
        DESIGN.model_label{model_nr} = modelsnames{model_nr};
        
        %%find all contrasts for this model
        
        contrasts = dir([DESIGN.Paths.outputFirst DESIGN.model_label{model_nr} '\ID' num2str(DESIGN.Participants(1)) '\con_000*']);
        
        for c = 1:length(contrasts)
            
            DESIGN.contrastNr = c;
            DESIGN.contrast = c;
            load(['D:\Anne\2_analysis\1_univariate\1_1st-Level\1_event\' DESIGN.model_label{model_nr} '\ID' num2str(DESIGN.Participants(1)) '\SPM.mat'])
            DESIGN.contrastName = SPM.xCon(1,DESIGN.contrastNr).name;
            DESIGN.imgNames = ['\con_000' num2str(DESIGN.contrastNr) '.nii'];
            
            DESIGN.Paths.outputSec = [pwd '\1_univariate\2_2nd-level\' DESIGN.dur '\'  DESIGN.model_label{model_nr} '\' DESIGN.contrastName  '\'];
            if ~exist(DESIGN.Paths.outputSec)
                mkdir(DESIGN.Paths.outputSec)
                
                %% specify
                % List of open inputs
                nrun = 1; % enter the number of runs here
                jobfile = {[DESIGN.Paths.analysisJobs '\specify_job.m']};
                jobs = repmat(jobfile, 1, nrun);
                inputs = cell(0, nrun);
                for crun = 1:nrun
                end
                spm('defaults', 'FMRI');
                spm_jobman('run', jobs, inputs{:});
                
                %% estimate
                % List of open inputs
                nrun = 1; % enter the number of runs here
                jobfile = {[DESIGN.Paths.analysisJobs '\estimate_job.m']};
                jobs = repmat(jobfile, 1, nrun);
                inputs = cell(0, nrun);
                for crun = 1:nrun
                end
                spm('defaults', 'FMRI');
                spm_jobman('run', jobs, inputs{:});
                
                %% define contrasts
                nrun = 1;
                jobfile = {[DESIGN.Paths.analysisJobs '\contrasts_job.m']};
                jobs = repmat(jobfile, 1, nrun);
                inputs = cell(1, nrun);
                for crun = 1:nrun
                end
                spm('defaults', 'FMRI');
                spm_jobman('run', jobs, inputs{:});
                
                %% results report
                % List of open inputs
                nrun = 1;
                jobfile = {[DESIGN.Paths.analysisJobs '\results_job.m']};
                jobs = repmat(jobfile, 1, nrun);
                inputs = cell(0, nrun);
                for crun = 1:nrun
                end
                spm('defaults', 'FMRI');
                spm_jobman('run', jobs, inputs{:});
                
                cd(DESIGN.Paths.analysis)
            end
        end
    end
end